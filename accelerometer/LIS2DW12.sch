EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9F7
P 2900 1550
AR Path="/60A6B9F7" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9F7" Ref="TP3"  Part="1" 
F 0 "TP3" V 2900 1738 50  0000 L CNN
F 1 "TestPoint" H 2958 1577 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 1550 50  0001 C CNN
F 3 "~" H 3100 1550 50  0001 C CNN
	1    2900 1550
	0    1    -1   0   
$EndComp
Wire Wire Line
	2900 1550 2750 1550
Wire Wire Line
	2900 1650 2750 1650
Wire Wire Line
	2900 1750 2750 1750
Text Label 2750 1550 2    50   ~ 0
INT1
Text Label 2750 1650 2    50   ~ 0
INT2
Text Label 2750 1750 2    50   ~ 0
SDO
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE01
P 2900 1750
AR Path="/6089BE01" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE01" Ref="TP5"  Part="1" 
F 0 "TP5" V 2900 1938 50  0000 L CNN
F 1 "TestPoint" H 2958 1777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 1750 50  0001 C CNN
F 3 "~" H 3100 1750 50  0001 C CNN
	1    2900 1750
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE07
P 2900 1650
AR Path="/6089BE07" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE07" Ref="TP4"  Part="1" 
F 0 "TP4" V 2900 1838 50  0000 L CNN
F 1 "TestPoint" H 2958 1677 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 1650 50  0001 C CNN
F 3 "~" H 3100 1650 50  0001 C CNN
	1    2900 1650
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9FA
P 2900 1850
AR Path="/60A6B9FA" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9FA" Ref="TP6"  Part="1" 
F 0 "TP6" V 2900 2038 50  0000 L CNN
F 1 "TestPoint" H 2958 1877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 1850 50  0001 C CNN
F 3 "~" H 3100 1850 50  0001 C CNN
	1    2900 1850
	0    1    -1   0   
$EndComp
Wire Wire Line
	2900 1850 2750 1850
Wire Wire Line
	2900 1950 2750 1950
Wire Wire Line
	2900 2050 2750 2050
$Comp
L Connector:TestPoint TP?
U 1 1 60A6B9FB
P 2900 2050
AR Path="/60A6B9FB" Ref="TP?"  Part="1" 
AR Path="/60886A89/60A6B9FB" Ref="TP8"  Part="1" 
F 0 "TP8" V 2900 2238 50  0000 L CNN
F 1 "TestPoint" H 2958 2077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 2050 50  0001 C CNN
F 3 "~" H 3100 2050 50  0001 C CNN
	1    2900 2050
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6089BE1C
P 2900 1950
AR Path="/6089BE1C" Ref="TP?"  Part="1" 
AR Path="/60886A89/6089BE1C" Ref="TP7"  Part="1" 
F 0 "TP7" V 2900 2138 50  0000 L CNN
F 1 "TestPoint" H 2958 1977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3100 1950 50  0001 C CNN
F 3 "~" H 3100 1950 50  0001 C CNN
	1    2900 1950
	0    1    -1   0   
$EndComp
Text Label 2750 1850 2    50   ~ 0
CS
Text Label 2750 1950 2    50   ~ 0
SDI
Text Label 2750 2050 2    50   ~ 0
SCL
Text HLabel 1550 2150 0    50   Input ~ 0
CS_ACC
Text Label 1700 2350 0    50   ~ 0
SDI
Wire Wire Line
	1550 2250 1700 2250
Wire Wire Line
	1550 2150 1700 2150
Wire Wire Line
	1550 2050 1700 2050
Text Label 1700 2050 0    50   ~ 0
SCL
Text Label 1700 2150 0    50   ~ 0
CS
Wire Wire Line
	1550 2350 1700 2350
Text Label 1700 2250 0    50   ~ 0
SDO
Wire Wire Line
	1550 2450 1700 2450
Wire Wire Line
	1550 2550 1700 2550
Text Label 1700 2450 0    50   ~ 0
INT1
Text Label 1700 2550 0    50   ~ 0
INT2
Text HLabel 1550 2050 0    50   Input ~ 0
SCL_ACC
Text HLabel 1550 2250 0    50   Output ~ 0
SDO_ACC
Text HLabel 1550 2350 0    50   Input ~ 0
SDI_ACC
Text HLabel 1550 2450 0    50   Output ~ 0
INT1_ACC
Text HLabel 1550 2550 0    50   Output ~ 0
INT2_ACC
Wire Wire Line
	1550 1850 1700 1850
Text Label 1700 1850 0    50   ~ 0
GND
Text HLabel 1550 1850 0    50   Input ~ 0
GND_ACC
Wire Wire Line
	1550 1750 1700 1750
Text Label 1700 1750 0    50   ~ 0
VCC_IO
Text HLabel 1550 1750 0    50   Input ~ 0
VDD_ACC_IO
Wire Wire Line
	1550 1650 1700 1650
Text Label 1700 1650 0    50   ~ 0
VCC_A
Text HLabel 1550 1650 0    50   Input ~ 0
VDD_ACC
Text Label 4900 3500 2    50   ~ 0
SDI
Wire Wire Line
	5050 3400 4900 3400
Wire Wire Line
	5050 3300 4900 3300
Wire Wire Line
	5050 3200 4900 3200
Text Label 4900 3200 2    50   ~ 0
SCL
Text Label 4900 3300 2    50   ~ 0
CS
Wire Wire Line
	5050 3500 4900 3500
Text Label 4900 3400 2    50   ~ 0
SDO
Wire Wire Line
	5550 4100 5550 4250
Wire Wire Line
	5650 4100 5650 4250
Wire Wire Line
	5550 4250 5650 4250
Wire Wire Line
	5550 4250 5450 4250
Connection ~ 5550 4250
Wire Wire Line
	5450 4100 5450 4250
Wire Wire Line
	6150 3600 6300 3600
Wire Wire Line
	6150 3700 6300 3700
Text Label 6300 3600 0    50   ~ 0
INT1
Text Label 6300 3700 0    50   ~ 0
INT2
$Comp
L Device:C C?
U 1 1 611AE484
P 6700 3150
AR Path="/611AE484" Ref="C?"  Part="1" 
AR Path="/60886A89/611AE484" Ref="C2"  Part="1" 
F 0 "C2" H 6815 3196 50  0000 L CNN
F 1 "10uF" H 6815 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6738 3000 50  0001 C CNN
F 3 "~" H 6700 3150 50  0001 C CNN
	1    6700 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611AE487
P 7150 3150
AR Path="/611AE487" Ref="C?"  Part="1" 
AR Path="/60886A89/611AE487" Ref="C3"  Part="1" 
F 0 "C3" H 7265 3196 50  0000 L CNN
F 1 "100nF" H 7265 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7188 3000 50  0001 C CNN
F 3 "~" H 7150 3150 50  0001 C CNN
	1    7150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3000 6700 2850
Wire Wire Line
	6700 2850 7150 2850
Wire Wire Line
	7150 3000 7150 2850
Wire Wire Line
	6700 3300 6700 3450
Wire Wire Line
	7150 3300 7150 3450
$Comp
L Device:C C?
U 1 1 611AE485
P 4150 3150
AR Path="/611AE485" Ref="C?"  Part="1" 
AR Path="/60886A89/611AE485" Ref="C1"  Part="1" 
F 0 "C1" H 4265 3196 50  0000 L CNN
F 1 "100nF" H 4265 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4188 3000 50  0001 C CNN
F 3 "~" H 4150 3150 50  0001 C CNN
	1    4150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3000 4150 2850
Wire Wire Line
	4150 3300 4150 3450
Text Label 4150 3450 0    50   ~ 0
GND
Wire Wire Line
	5850 2850 5850 2900
Connection ~ 6700 2850
Wire Wire Line
	5850 2850 6250 2850
Text Notes 3850 3600 0    59   ~ 0
Near to VDD_IO
Text Notes 6650 3600 0    59   ~ 0
Near to VDD
Wire Wire Line
	5750 2850 5750 2900
$Comp
L power:PWR_FLAG #FLG?
U 1 1 611AE486
P 6250 2700
AR Path="/611AE486" Ref="#FLG?"  Part="1" 
AR Path="/60886A89/611AE486" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 6250 2775 50  0001 C CNN
F 1 "PWR_FLAG" H 6250 2873 50  0000 C CNN
F 2 "" H 6250 2700 50  0001 C CNN
F 3 "~" H 6250 2700 50  0001 C CNN
	1    6250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2700 6250 2850
Connection ~ 6250 2850
Wire Wire Line
	6250 2850 6700 2850
Text Label 6550 2850 0    50   ~ 0
VCC_A
Wire Wire Line
	4150 2850 5750 2850
Wire Wire Line
	5350 4100 5350 4250
Wire Wire Line
	5350 4250 5450 4250
Connection ~ 5450 4250
Text Label 5050 2850 2    50   ~ 0
VCC_IO
$Comp
L custom:LIS2DW12 U2
U 1 1 611AE7B5
P 5650 3500
F 0 "U2" H 6050 3850 60  0000 L CNN
F 1 "LIS2DW12" H 6050 3700 60  0000 L CNN
F 2 "digikey-footprints:VFLGA-12_2x2mm" H 5850 3700 60  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/12/c0/5c/36/b9/58/46/f2/DM00091513.pdf/files/DM00091513.pdf/jcr:content/translations/en.DM00091513.pdf" H 5850 3800 60  0001 L CNN
	1    5650 3500
	1    0    0    -1  
$EndComp
Text Label 6700 3450 0    50   ~ 0
GND
Text Label 7150 3450 0    50   ~ 0
GND
Text Label 5400 4250 0    50   ~ 0
GND
$EndSCHEMATC
