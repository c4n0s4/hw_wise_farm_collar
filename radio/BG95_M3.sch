EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Radio Module"
Date "2021-04-21"
Rev "1"
Comp ""
Comment1 "CATM/NB_IoT  Module "
Comment2 "Vitor Canosa"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6650 2800 6500 2800
$Comp
L Device:C C?
U 1 1 6171B4D6
P 8550 2700
AR Path="/6171B4D6" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4D6" Ref="C16"  Part="1" 
AR Path="/6114C59C/6171B4D6" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4D6" Ref="C49"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4D6" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4D6" Ref="C36"  Part="1" 
F 0 "C36" H 8665 2746 50  0000 L CNN
F 1 "100nF" H 8665 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8588 2550 50  0001 C CNN
F 3 "~" H 8550 2700 50  0001 C CNN
	1    8550 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6171B4D7
P 9000 2700
AR Path="/6171B4D7" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4D7" Ref="C17"  Part="1" 
AR Path="/6114C59C/6171B4D7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4D7" Ref="C52"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4D7" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4D7" Ref="C39"  Part="1" 
F 0 "C39" H 9115 2746 50  0000 L CNN
F 1 "100pF" H 9115 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9038 2550 50  0001 C CNN
F 3 "~" H 9000 2700 50  0001 C CNN
	1    9000 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2550 8550 2400
Wire Wire Line
	9000 2550 9000 2400
Wire Wire Line
	8550 2850 8550 3000
Wire Wire Line
	9000 2850 9000 3000
$Comp
L Device:C C?
U 1 1 6171B4DA
P 9500 2700
AR Path="/6171B4DA" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4DA" Ref="C20"  Part="1" 
AR Path="/6114C59C/6171B4DA" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4DA" Ref="C54"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4DA" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4DA" Ref="C41"  Part="1" 
F 0 "C41" H 9615 2746 50  0000 L CNN
F 1 "22pF" H 9615 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9538 2550 50  0001 C CNN
F 3 "~" H 9500 2700 50  0001 C CNN
	1    9500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2550 9500 2400
Wire Wire Line
	9500 2850 9500 3000
Wire Wire Line
	8550 3000 9000 3000
Wire Wire Line
	9000 3000 9500 3000
Connection ~ 9000 3000
Wire Wire Line
	8550 2400 9000 2400
Wire Wire Line
	9500 2400 9000 2400
Connection ~ 9000 2400
Text HLabel 1150 750  0    50   Input ~ 0
VCC_R
Text HLabel 1150 850  0    50   Input ~ 0
GND_R
Wire Wire Line
	1150 750  1300 750 
Wire Wire Line
	1150 850  1300 850 
Text Label 1300 750  0    50   ~ 0
VCC_R
Text Label 1300 850  0    50   ~ 0
GND_R
Text Label 6650 2800 0    50   ~ 0
GND_R
Text Label 8700 2400 0    50   ~ 0
VCC_R
Text Label 8700 3000 0    50   ~ 0
GND_R
Text HLabel 1100 1950 0    50   Input ~ 0
PWRKEY_R
Wire Wire Line
	1100 1950 1250 1950
Text HLabel 4350 750  0    50   Output ~ 0
TXD_R
Text HLabel 4350 850  0    50   Input ~ 0
RXD_R
Wire Wire Line
	4350 750  4500 750 
Wire Wire Line
	4350 850  4500 850 
Text Label 6650 3200 0    50   ~ 0
USIM_RST
Text Label 6650 3300 0    50   ~ 0
USIM_VDD
Text Label 6650 3000 0    50   ~ 0
USIM_CLK
Text Label 6650 3100 0    50   ~ 0
USIM_DATA
Text Notes 7650 2250 0    50   ~ 0
The input voltage of VBAT ranges from 3.3V to 4.2V for BG95-M3. Typical voltage of 3.8V.\nThe module drains a maximum current of approx. 1.6A during GSM burst.\nThe minimum width of VBAT trace is recommended to be 2mm.
$Comp
L Device:C C?
U 1 1 60ED0266
P 9900 5200
AR Path="/60ED0266" Ref="C?"  Part="1" 
AR Path="/60C56809/60ED0266" Ref="C10"  Part="1" 
AR Path="/6114C59C/60ED0266" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60ED0266" Ref="C43"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60ED0266" Ref="C?"  Part="1" 
AR Path="/60BA0495/60ED0266" Ref="C30"  Part="1" 
F 0 "C30" H 10015 5246 50  0000 L CNN
F 1 "4.7uF" H 10015 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9938 5050 50  0001 C CNN
F 3 "~" H 9900 5200 50  0001 C CNN
	1    9900 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 5350 9900 5500
Wire Wire Line
	9900 5050 9900 4900
Text Label 9900 5500 0    50   ~ 0
GND_R
Text Label 9900 4900 0    50   ~ 0
VDD_EXT
Text Notes 9300 4900 0    50   ~ 0
It is recommended to use VDD_EXT for \nthe external pull-up power supply for I/O\nUsed for external pull up circuits, and\nneed to add a 2.2~4.7uF bypass\ncapacitor in\nparallel.
Text Notes 7800 3350 0    50   ~ 0
These capacitor are arranged with capacitances in ascending order. \nCapacitor with lowest capacitance is closest to VBAT pin and all \ncapacitors should be as close to VBAT pin as possible.\n
Text Notes 1150 2600 0    50   ~ 0
Reset Reference Circuit
Text HLabel 1100 1800 0    50   Output ~ 0
UCR_R
Wire Wire Line
	1100 1800 1250 1800
Text Label 1250 1800 0    50   ~ 0
NB_URC
Wire Wire Line
	2500 650  2350 650 
Wire Wire Line
	2500 800  2350 800 
$Comp
L Connector:TestPoint TP?
U 1 1 6171B4DB
P 2500 800
AR Path="/6171B4DB" Ref="TP?"  Part="1" 
AR Path="/60C56809/6171B4DB" Ref="TP9"  Part="1" 
AR Path="/6114C59C/6171B4DB" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6171B4DB" Ref="TP29"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4DB" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6171B4DB" Ref="TP24"  Part="1" 
F 0 "TP24" V 2500 988 50  0000 L CNN
F 1 "TestPoint" H 2558 827 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 800 50  0001 C CNN
F 3 "~" H 2700 800 50  0001 C CNN
	1    2500 800 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61D38F09
P 2500 650
AR Path="/61D38F09" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38F09" Ref="TP2"  Part="1" 
AR Path="/6114C59C/61D38F09" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38F09" Ref="TP28"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F09" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38F09" Ref="TP23"  Part="1" 
F 0 "TP23" V 2500 838 50  0000 L CNN
F 1 "TestPoint" H 2558 677 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2700 650 50  0001 C CNN
F 3 "~" H 2700 650 50  0001 C CNN
	1    2500 650 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6171B4CF
P 2500 900
AR Path="/6171B4CF" Ref="TP?"  Part="1" 
AR Path="/60C56809/6171B4CF" Ref="TP10"  Part="1" 
AR Path="/6114C59C/6171B4CF" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6171B4CF" Ref="TP30"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4CF" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6171B4CF" Ref="TP25"  Part="1" 
F 0 "TP25" V 2500 1088 50  0000 L CNN
F 1 "TestPoint" H 2558 927 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 900 50  0001 C CNN
F 3 "~" H 2700 900 50  0001 C CNN
	1    2500 900 
	0    1    -1   0   
$EndComp
Wire Wire Line
	2500 900  2350 900 
Text Label 2350 650  2    50   ~ 0
RESET_R
Text Label 2350 800  2    50   ~ 0
TXD_R
Text Label 2350 900  2    50   ~ 0
RXD_R
Text Notes 4050 650  0    50   ~ 0
(MCU_TXD / MCU_RXD)
Wire Wire Line
	2500 1050 2350 1050
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8DF
P 2500 1050
AR Path="/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8DF" Ref="TP13"  Part="1" 
AR Path="/6114C59C/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6132A8DF" Ref="TP33"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6132A8DF" Ref="TP32"  Part="1" 
F 0 "TP32" V 2500 1238 50  0000 L CNN
F 1 "TestPoint" H 2558 1077 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1050 50  0001 C CNN
F 3 "~" H 2700 1050 50  0001 C CNN
	1    2500 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	2500 1200 2350 1200
Wire Wire Line
	2500 1300 2350 1300
$Comp
L Connector:TestPoint TP?
U 1 1 61D38EFD
P 2500 1300
AR Path="/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38EFD" Ref="TP16"  Part="1" 
AR Path="/6114C59C/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38EFD" Ref="TP36"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38EFD" Ref="TP46"  Part="1" 
F 0 "TP46" V 2500 1488 50  0000 L CNN
F 1 "TestPoint" H 2558 1327 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1300 50  0001 C CNN
F 3 "~" H 2700 1300 50  0001 C CNN
	1    2500 1300
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61D38EFE
P 2500 1200
AR Path="/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38EFE" Ref="TP15"  Part="1" 
AR Path="/6114C59C/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38EFE" Ref="TP35"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38EFE" Ref="TP45"  Part="1" 
F 0 "TP45" V 2500 1388 50  0000 L CNN
F 1 "TestPoint" H 2558 1227 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2700 1200 50  0001 C CNN
F 3 "~" H 2700 1200 50  0001 C CNN
	1    2500 1200
	0    1    -1   0   
$EndComp
Text Label 6650 3750 0    50   ~ 0
NB_URC
$Comp
L Device:R R?
U 1 1 6171B4DF
P 1000 6700
AR Path="/6171B4DF" Ref="R?"  Part="1" 
AR Path="/60C56809/6171B4DF" Ref="R28"  Part="1" 
AR Path="/6114C59C/6171B4DF" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/6171B4DF" Ref="R65"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4DF" Ref="R?"  Part="1" 
AR Path="/60BA0495/6171B4DF" Ref="R37"  Part="1" 
F 0 "R37" H 930 6654 50  0000 R CNN
F 1 "10k" H 930 6745 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 930 6700 50  0001 C CNN
F 3 "~" H 1000 6700 50  0001 C CNN
	1    1000 6700
	1    0    0    1   
$EndComp
Wire Wire Line
	1000 6850 1000 7000
$Comp
L Device:C C?
U 1 1 6171B4D4
P 3900 6900
AR Path="/6171B4D4" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4D4" Ref="C23"  Part="1" 
AR Path="/6114C59C/6171B4D4" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4D4" Ref="C53"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6171B4D4" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4D4" Ref="C40"  Part="1" 
F 0 "C40" H 4015 6946 50  0000 L CNN
F 1 "100nF" H 4015 6855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3938 6750 50  0001 C CNN
F 3 "~" H 3900 6900 50  0001 C CNN
	1    3900 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 7050 3900 7200
Text Label 5850 5500 3    50   ~ 0
GND_R
Wire Wire Line
	5850 5350 5850 5500
Text Label 5950 5500 3    50   ~ 0
GND_R
Wire Wire Line
	5950 5350 5950 5500
Text Label 6050 5500 3    50   ~ 0
GND_R
Wire Wire Line
	6050 5350 6050 5500
Wire Wire Line
	6650 4800 6500 4800
Text Label 6650 4800 0    50   ~ 0
GND_R
Wire Wire Line
	6650 4700 6500 4700
Text Label 6650 4700 0    50   ~ 0
GND_R
Text Notes 3150 900  0    50   ~ 0
BC_95\nVmax=4.2V\nVmin=3.1V\nVnorm=3.6V
Wire Wire Line
	10300 1100 10450 1100
Text Label 10300 1100 0    60   ~ 0
ANT
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 6171B4CC
P 9700 1400
AR Path="/6171B4CC" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4CC" Ref="C?"  Part="1" 
AR Path="/6114C59C/6171B4CC" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4CC" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6171B4CC" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4CC" Ref="C4"  Part="1" 
F 0 "C4" H 9800 1400 50  0000 L CNN
F 1 "X pF" H 9725 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9738 1250 30  0001 C CNN
F 3 "Kit smd capacitors" H 9700 1400 60  0001 C CNN
F 4 "---" H 9700 1400 60  0001 C CNN "digikey"
	1    9700 1400
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 6171B4CB
P 10300 1400
AR Path="/6171B4CB" Ref="C?"  Part="1" 
AR Path="/60C56809/6171B4CB" Ref="C?"  Part="1" 
AR Path="/6114C59C/6171B4CB" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6171B4CB" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6171B4CB" Ref="C?"  Part="1" 
AR Path="/60BA0495/6171B4CB" Ref="C8"  Part="1" 
F 0 "C8" H 10050 1400 50  0000 L CNN
F 1 "X pF" H 9950 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10338 1250 30  0001 C CNN
F 3 "Kit smd capacitors" H 10300 1400 60  0001 C CNN
F 4 "---" H 10300 1400 60  0001 C CNN "digikey"
	1    10300 1400
	-1   0    0    -1  
$EndComp
Text Notes 10350 850  2    60   ~ 0
PI Macthing Network Main
$Comp
L wise_farm_nb_iot_2-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue L?
U 1 1 61D38EEA
P 10000 1100
AR Path="/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60C56809/61D38EEA" Ref="L?"  Part="1" 
AR Path="/6114C59C/61D38EEA" Ref="L?"  Part="1" 
AR Path="/61CCC1A5/61D38EEA" Ref="L?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60BA0495/61D38EEA" Ref="L2"  Part="1" 
F 0 "L2" V 9950 1100 50  0000 C CNN
F 1 "L" V 10075 1100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 10000 1100 50  0001 C CNN
F 3 "Kit smd inductors" H 10000 1100 50  0001 C CNN
F 4 "---" V 10000 1100 60  0001 C CNN "digikey"
	1    10000 1100
	0    1    -1   0   
$EndComp
Text Label 9550 1100 2    50   ~ 0
ANT_IMN_MAIN
Wire Wire Line
	10300 1550 10300 1700
Wire Wire Line
	9700 1550 9700 1700
Wire Wire Line
	9700 1250 9700 1100
Wire Wire Line
	9700 1100 9850 1100
Wire Wire Line
	10300 1250 10300 1100
Wire Wire Line
	10300 1100 10150 1100
Connection ~ 10300 1100
Text Label 9700 1700 0    50   ~ 0
GND_R
Text Label 10300 1700 0    50   ~ 0
GND_R
Wire Wire Line
	9700 1100 9550 1100
Connection ~ 9700 1100
$Comp
L custom:ANT_CUSTOM U?
U 1 1 609AB7B3
P 10850 1100
AR Path="/60B4409A/609AB7B3" Ref="U?"  Part="1" 
AR Path="/60C56809/609AB7B3" Ref="U?"  Part="1" 
AR Path="/60BA0495/609AB7B3" Ref="U4"  Part="1" 
F 0 "U4" H 10869 1235 50  0000 L CNN
F 1 "ANT_CUSTOM" H 10850 1200 50  0001 C CNN
F 2 "custom:antenna_DUAL_BAND_NB_IoT_B28_B3" H 10850 1100 50  0001 C CNN
F 3 "" H 10850 1100 50  0001 C CNN
	1    10850 1100
	1    0    0    -1  
$EndComp
Text Label 2350 1300 2    50   ~ 0
USIM_VDD
Text Label 2350 1200 2    50   ~ 0
USIM_DATA
Text Label 2350 1050 2    50   ~ 0
USIM_RST
Text Label 1000 7000 2    50   ~ 0
USIM_DATA
Wire Wire Line
	9950 2550 9950 2400
Wire Wire Line
	9500 2400 9950 2400
Connection ~ 9500 2400
Wire Wire Line
	9950 3000 9500 3000
Connection ~ 9500 3000
$Comp
L custom:BG95_M3 U6
U 1 1 61264841
P 4900 3650
F 0 "U6" H 4900 3750 50  0000 C CNN
F 1 "BG95_M3" H 4900 3550 50  0000 C CNN
F 2 "custom:BG95M3" H 3350 2500 50  0001 C CNN
F 3 "" H 3350 2500 50  0001 C CNN
	1    4900 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2900 6650 2900
Wire Wire Line
	6500 3000 6650 3000
Wire Wire Line
	6500 3100 6650 3100
Wire Wire Line
	6500 3200 6650 3200
Wire Wire Line
	6500 3300 6650 3300
Wire Wire Line
	6500 3750 6650 3750
Text Label 5550 5500 3    50   ~ 0
GND_R
Wire Wire Line
	5550 5350 5550 5500
Text Label 3700 5500 3    50   ~ 0
GND_R
Wire Wire Line
	3700 5350 3700 5500
Text Label 3800 5500 3    50   ~ 0
GND_R
Wire Wire Line
	3800 5350 3800 5500
Text Label 3900 5500 3    50   ~ 0
GND_R
Wire Wire Line
	3900 5350 3900 5500
Text Label 4000 5500 3    50   ~ 0
GND_R
Wire Wire Line
	4000 5350 4000 5500
Wire Wire Line
	4950 5350 4950 5500
Wire Wire Line
	5050 5350 5050 5500
Wire Wire Line
	5150 5350 5150 5500
Wire Wire Line
	5250 5350 5250 5500
Wire Wire Line
	5450 5350 5450 5500
Wire Wire Line
	4750 5350 4750 5500
Wire Wire Line
	4650 5350 4650 5500
Wire Wire Line
	4550 5350 4550 5500
Wire Wire Line
	4450 5350 4450 5500
Wire Wire Line
	4350 5350 4350 5500
Wire Wire Line
	4250 5350 4250 5500
Wire Wire Line
	6500 3850 6650 3850
Wire Wire Line
	6500 3950 6650 3950
Wire Wire Line
	6500 4050 6650 4050
Wire Wire Line
	6500 4150 6650 4150
Wire Wire Line
	6500 4250 6650 4250
Wire Wire Line
	6500 4350 6650 4350
Wire Wire Line
	6500 4450 6650 4450
Wire Wire Line
	6500 3600 6650 3600
Wire Wire Line
	6500 3500 6650 3500
Wire Wire Line
	6500 3400 6650 3400
Wire Wire Line
	3150 4700 3300 4700
Wire Wire Line
	3150 4800 3300 4800
Wire Wire Line
	3150 4450 3300 4450
Wire Wire Line
	3150 4350 3300 4350
Wire Wire Line
	3150 4150 3300 4150
Wire Wire Line
	3150 3500 3300 3500
Wire Wire Line
	3150 3400 3300 3400
Wire Wire Line
	3150 3300 3300 3300
Wire Wire Line
	3150 3200 3300 3200
Wire Wire Line
	3150 3100 3300 3100
Wire Wire Line
	3150 3000 3300 3000
Wire Wire Line
	3150 2900 3300 2900
Wire Wire Line
	3150 2800 3300 2800
Wire Wire Line
	3150 2700 3300 2700
Wire Wire Line
	3150 2600 3300 2600
Wire Wire Line
	3700 1950 3700 1800
Wire Wire Line
	3800 1950 3800 1800
Wire Wire Line
	3900 1950 3900 1800
Wire Wire Line
	4000 1950 4000 1800
Wire Wire Line
	4250 1950 4250 1800
Wire Wire Line
	4350 1950 4350 1800
Wire Wire Line
	4450 1950 4450 1800
Wire Wire Line
	4550 1950 4550 1800
Wire Wire Line
	4950 1950 4950 1800
Wire Wire Line
	5050 1950 5050 1800
Wire Wire Line
	5150 1950 5150 1800
Wire Wire Line
	5750 1950 5750 1800
Wire Wire Line
	5850 1950 5850 1800
Wire Wire Line
	5950 1950 5950 1800
Wire Wire Line
	5550 1950 5550 1800
Wire Wire Line
	4650 1950 4650 1800
NoConn ~ 4950 5500
NoConn ~ 5050 5500
NoConn ~ 4750 5500
NoConn ~ 4650 5500
NoConn ~ 4550 5500
NoConn ~ 5450 5500
NoConn ~ 6650 4050
NoConn ~ 6650 3950
NoConn ~ 6650 3850
NoConn ~ 6650 3600
NoConn ~ 6650 3500
NoConn ~ 6650 3400
NoConn ~ 6500 2500
Text Label 5950 1800 1    50   ~ 0
GND_R
Text Label 5850 1800 1    50   ~ 0
GND_R
Text Label 5750 1800 1    50   ~ 0
GND_R
Text Label 5550 1800 1    50   ~ 0
GND_R
Text Label 5150 1800 1    50   ~ 0
GND_R
Text Label 5050 1800 1    50   ~ 0
GND_R
Text Label 4650 1800 1    50   ~ 0
GND_R
Text Label 4550 1800 1    50   ~ 0
GND_R
Text Label 4350 1800 1    50   ~ 0
GND_R
Text Label 4250 1800 1    50   ~ 0
GND_R
Text Label 4000 1800 1    50   ~ 0
GND_R
Text Label 3900 1800 1    50   ~ 0
GND_R
Text Label 3800 1800 1    50   ~ 0
GND_R
Text Label 3700 1800 1    50   ~ 0
GND_R
Text Label 3150 2800 2    50   ~ 0
GND_R
NoConn ~ 3150 2900
NoConn ~ 3150 3000
NoConn ~ 3150 3100
NoConn ~ 3150 3200
NoConn ~ 3150 3300
NoConn ~ 3150 3400
NoConn ~ 3150 3500
Text Label 3150 4700 2    50   ~ 0
GND_R
Text Label 3150 4800 2    50   ~ 0
GND_R
Text Label 6650 4150 0    50   ~ 0
TXD_R
Text Label 6650 4250 0    50   ~ 0
RXD_R
Text Label 4500 750  0    50   ~ 0
TXD_R
Text Label 4500 850  0    50   ~ 0
RXD_R
NoConn ~ 3150 2600
NoConn ~ 3150 2700
Text Label 3150 4350 2    50   ~ 0
RESET_N
Text Label 3150 4150 2    50   ~ 0
PWRKEY
Text Label 2150 4900 1    50   ~ 0
PWRKEY
Text Label 1250 1950 0    50   ~ 0
PWRKEY_R
Text Label 1100 5250 2    50   ~ 0
PWRKEY_R
Text Label 4450 1800 1    50   ~ 0
ANT_IMN_MAIN
Wire Wire Line
	6500 2700 6650 2700
Text Label 6650 2700 0    50   ~ 0
ANT_IMN_GNSS
Wire Wire Line
	7950 1100 8100 1100
Text Label 7950 1100 0    60   ~ 0
ANT_GPS
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 615C7080
P 7350 1400
AR Path="/615C7080" Ref="C?"  Part="1" 
AR Path="/60C56809/615C7080" Ref="C?"  Part="1" 
AR Path="/6114C59C/615C7080" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/615C7080" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/615C7080" Ref="C?"  Part="1" 
AR Path="/60BA0495/615C7080" Ref="C15"  Part="1" 
F 0 "C15" H 7450 1400 50  0000 L CNN
F 1 "X pF" H 7375 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7388 1250 30  0001 C CNN
F 3 "Kit smd capacitors" H 7350 1400 60  0001 C CNN
F 4 "---" H 7350 1400 60  0001 C CNN "digikey"
	1    7350 1400
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 615C7087
P 7950 1400
AR Path="/615C7087" Ref="C?"  Part="1" 
AR Path="/60C56809/615C7087" Ref="C?"  Part="1" 
AR Path="/6114C59C/615C7087" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/615C7087" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/615C7087" Ref="C?"  Part="1" 
AR Path="/60BA0495/615C7087" Ref="C16"  Part="1" 
F 0 "C16" H 7700 1400 50  0000 L CNN
F 1 "X pF" H 7600 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7988 1250 30  0001 C CNN
F 3 "Kit smd capacitors" H 7950 1400 60  0001 C CNN
F 4 "---" H 7950 1400 60  0001 C CNN "digikey"
	1    7950 1400
	-1   0    0    -1  
$EndComp
Text Notes 8000 850  2    60   ~ 0
PI Macthing Network GNSS
$Comp
L wise_farm_nb_iot_2-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue L?
U 1 1 615C708F
P 7650 1100
AR Path="/615C708F" Ref="L?"  Part="1" 
AR Path="/60C56809/615C708F" Ref="L?"  Part="1" 
AR Path="/6114C59C/615C708F" Ref="L?"  Part="1" 
AR Path="/61CCC1A5/615C708F" Ref="L?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/615C708F" Ref="L?"  Part="1" 
AR Path="/60BA0495/615C708F" Ref="L3"  Part="1" 
F 0 "L3" V 7600 1100 50  0000 C CNN
F 1 "L" V 7725 1100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 7650 1100 50  0001 C CNN
F 3 "Kit smd inductors" H 7650 1100 50  0001 C CNN
F 4 "---" V 7650 1100 60  0001 C CNN "digikey"
	1    7650 1100
	0    1    -1   0   
$EndComp
Text Label 7200 1100 2    50   ~ 0
ANT_IMN_GNSS
Wire Wire Line
	7950 1550 7950 1700
Wire Wire Line
	7350 1550 7350 1700
Wire Wire Line
	7350 1250 7350 1100
Wire Wire Line
	7350 1100 7500 1100
Wire Wire Line
	7950 1250 7950 1100
Wire Wire Line
	7950 1100 7800 1100
Connection ~ 7950 1100
Text Label 7350 1700 0    50   ~ 0
GND_R
Text Label 7950 1700 0    50   ~ 0
GND_R
Wire Wire Line
	7350 1100 7200 1100
Connection ~ 7350 1100
$Comp
L custom:ANT_CUSTOM U?
U 1 1 615C70A1
P 8500 1100
AR Path="/60B4409A/615C70A1" Ref="U?"  Part="1" 
AR Path="/60C56809/615C70A1" Ref="U?"  Part="1" 
AR Path="/60BA0495/615C70A1" Ref="U8"  Part="1" 
F 0 "U8" H 8519 1235 50  0000 L CNN
F 1 "ANT_CUSTOM" H 8500 1200 50  0001 C CNN
F 2 "custom:antenna_GPS_L1_1575MHz_NB" H 8500 1100 50  0001 C CNN
F 3 "" H 8500 1100 50  0001 C CNN
	1    8500 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1950 5350 1800
Wire Wire Line
	5250 1950 5250 1800
Text Label 5350 5500 3    50   ~ 0
VDD_EXT
Wire Wire Line
	5350 5350 5350 5500
Text HLabel 5400 1100 0    50   Input ~ 0
RXD_GPS
Wire Wire Line
	5400 1100 5550 1100
Text Notes 5100 650  0    50   ~ 0
(MCU_TXD / MCU_RXD)
Text Label 5550 1100 0    50   ~ 0
RXD_GPS
Text Label 5250 5500 3    50   ~ 0
RXD_GPS
Text Label 5350 1800 1    50   ~ 0
VCC_R
NoConn ~ 4950 1800
NoConn ~ 3150 4450
NoConn ~ 4450 5500
NoConn ~ 4350 5500
NoConn ~ 4250 5500
Text Notes 1000 4450 0    50   ~ 0
PWRKEY Reference Circuit
Text HLabel 1100 2250 0    50   Input ~ 0
RESET_R
Wire Wire Line
	1100 2250 1250 2250
Text Label 1250 2250 0    50   ~ 0
RESET_R
$Comp
L Device:C C?
U 1 1 616E71D7
P 9450 3850
AR Path="/616E71D7" Ref="C?"  Part="1" 
AR Path="/60C56809/616E71D7" Ref="C?"  Part="1" 
AR Path="/6114C59C/616E71D7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/616E71D7" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/616E71D7" Ref="C?"  Part="1" 
AR Path="/60BA0495/616E71D7" Ref="C17"  Part="1" 
F 0 "C17" H 9565 3896 50  0000 L CNN
F 1 "100nF" H 9565 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9488 3700 50  0001 C CNN
F 3 "~" H 9450 3850 50  0001 C CNN
	1    9450 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 616E71DD
P 9900 3850
AR Path="/616E71DD" Ref="C?"  Part="1" 
AR Path="/60C56809/616E71DD" Ref="C?"  Part="1" 
AR Path="/6114C59C/616E71DD" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/616E71DD" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/616E71DD" Ref="C?"  Part="1" 
AR Path="/60BA0495/616E71DD" Ref="C18"  Part="1" 
F 0 "C18" H 10015 3896 50  0000 L CNN
F 1 "100pF" H 10015 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9938 3700 50  0001 C CNN
F 3 "~" H 9900 3850 50  0001 C CNN
	1    9900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 3700 9450 3550
Wire Wire Line
	9900 3700 9900 3550
Wire Wire Line
	9450 4000 9450 4150
Wire Wire Line
	9900 4000 9900 4150
$Comp
L Device:C C?
U 1 1 616E71E7
P 10400 3850
AR Path="/616E71E7" Ref="C?"  Part="1" 
AR Path="/60C56809/616E71E7" Ref="C?"  Part="1" 
AR Path="/6114C59C/616E71E7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/616E71E7" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/616E71E7" Ref="C?"  Part="1" 
AR Path="/60BA0495/616E71E7" Ref="C19"  Part="1" 
F 0 "C19" H 10515 3896 50  0000 L CNN
F 1 "22pF" H 10515 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10438 3700 50  0001 C CNN
F 3 "~" H 10400 3850 50  0001 C CNN
	1    10400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 3700 10400 3550
Wire Wire Line
	10400 4000 10400 4150
Wire Wire Line
	9450 4150 9900 4150
Wire Wire Line
	9900 4150 10400 4150
Connection ~ 9900 4150
Wire Wire Line
	9450 3550 9900 3550
Wire Wire Line
	10400 3550 9900 3550
Connection ~ 9900 3550
Text Label 9600 3550 0    50   ~ 0
VCC_R
Text Label 9600 4150 0    50   ~ 0
GND_R
$Comp
L Device:C C?
U 1 1 616E71F7
P 10850 3850
AR Path="/616E71F7" Ref="C?"  Part="1" 
AR Path="/60C56809/616E71F7" Ref="C?"  Part="1" 
AR Path="/6114C59C/616E71F7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/616E71F7" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/616E71F7" Ref="C?"  Part="1" 
AR Path="/60BA0495/616E71F7" Ref="C20"  Part="1" 
F 0 "C20" H 10965 3896 50  0000 L CNN
F 1 "47uF" H 10965 3805 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10888 3700 50  0001 C CNN
F 3 "~" H 10850 3850 50  0001 C CNN
	1    10850 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 3700 10850 3550
Wire Wire Line
	10850 4000 10850 4150
Wire Wire Line
	10400 3550 10850 3550
Connection ~ 10400 3550
Wire Wire Line
	10850 4150 10400 4150
Connection ~ 10400 4150
Text Label 1150 3450 2    50   ~ 0
RESET_R
Text Label 2200 3100 1    50   ~ 0
RESET_N
Text Label 6650 4350 0    50   ~ 0
VCC_R
Text Label 6650 4450 0    50   ~ 0
VCC_R
Text Label 5250 1800 1    50   ~ 0
VCC_R
Text Label 6650 2900 0    50   ~ 0
GND_R
$Comp
L custom:SIM-ST-MFF2 U9
U 1 1 614FCAD7
P 2300 6900
F 0 "U9" H 2300 7565 50  0000 C CNN
F 1 "SIM-MFF2" H 2300 7474 50  0000 C CNN
F 2 "custom:dakota_iot_MFF2" H 2300 6900 50  0001 L BNN
F 3 "" H 2300 6900 50  0001 L BNN
F 4 "Hologram" H 2300 6900 50  0001 L BNN "MANUFACTURER"
F 5 "IPC-7351B" H 2300 6900 50  0001 L BNN "STANDARD"
	1    2300 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6500 3050 6500
Wire Wire Line
	2900 7200 3050 7200
Wire Wire Line
	2900 7300 3050 7300
Text Notes 1650 6100 0    50   ~ 0
Embedded SIM CARD multi profile
Wire Wire Line
	1700 7000 1550 7000
Wire Wire Line
	1700 6800 1550 6800
Wire Wire Line
	1700 6700 1550 6700
Text Label 1550 6800 2    50   ~ 0
USIM_CLK
Text Label 1550 6700 2    50   ~ 0
USIM_RST
Text Label 1550 7000 2    50   ~ 0
USIM_DATA
NoConn ~ 3050 7200
Text Label 3050 7300 0    50   ~ 0
GND_R
Wire Wire Line
	3900 6750 3900 6600
Text Label 3900 7200 0    50   ~ 0
GND_R
Wire Wire Line
	1000 6550 1000 6400
$Comp
L Device:C C?
U 1 1 618151D0
P 4550 6900
AR Path="/618151D0" Ref="C?"  Part="1" 
AR Path="/60C56809/618151D0" Ref="C?"  Part="1" 
AR Path="/6114C59C/618151D0" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/618151D0" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/618151D0" Ref="C?"  Part="1" 
AR Path="/60BA0495/618151D0" Ref="C21"  Part="1" 
F 0 "C21" H 4665 6946 50  0000 L CNN
F 1 "1uF" H 4665 6855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 6750 50  0001 C CNN
F 3 "~" H 4550 6900 50  0001 C CNN
	1    4550 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 7050 4550 7200
Wire Wire Line
	4550 6750 4550 6600
Text Label 4550 7200 0    50   ~ 0
GND_R
Text Label 1000 6400 2    50   ~ 0
USIM_VDD
Text Label 3050 6500 0    50   ~ 0
USIM_VDD
Text Label 3900 6600 0    50   ~ 0
USIM_VDD
Text Label 4550 6600 0    50   ~ 0
USIM_VDD
Wire Wire Line
	5400 850  5550 850 
Text Label 5550 850  0    50   ~ 0
TXD_GPS
Text HLabel 5400 850  0    50   Output ~ 0
TXD_GPS
Text Label 5150 5500 3    50   ~ 0
TXD_GPS
Wire Wire Line
	9950 2850 9950 3000
$Comp
L Device:C C?
U 1 1 611C5B6A
P 9950 2700
AR Path="/611C5B6A" Ref="C?"  Part="1" 
AR Path="/60C56809/611C5B6A" Ref="C?"  Part="1" 
AR Path="/6114C59C/611C5B6A" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/611C5B6A" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/611C5B6A" Ref="C?"  Part="1" 
AR Path="/60BA0495/611C5B6A" Ref="C12"  Part="1" 
F 0 "C12" H 10065 2746 50  0000 L CNN
F 1 "47uF" H 10065 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 9988 2550 50  0001 C CNN
F 3 "~" H 9950 2700 50  0001 C CNN
	1    9950 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 2550 10400 2400
Wire Wire Line
	9950 2400 10400 2400
Wire Wire Line
	10400 3000 9950 3000
Wire Wire Line
	10400 2850 10400 3000
$Comp
L Device:C C?
U 1 1 619442F5
P 10400 2700
AR Path="/619442F5" Ref="C?"  Part="1" 
AR Path="/60C56809/619442F5" Ref="C?"  Part="1" 
AR Path="/6114C59C/619442F5" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/619442F5" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/619442F5" Ref="C?"  Part="1" 
AR Path="/60BA0495/619442F5" Ref="C22"  Part="1" 
F 0 "C22" H 10515 2746 50  0000 L CNN
F 1 "47uF" H 10515 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10438 2550 50  0001 C CNN
F 3 "~" H 10400 2700 50  0001 C CNN
	1    10400 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 2550 10850 2400
Wire Wire Line
	10400 2400 10850 2400
Wire Wire Line
	10850 3000 10400 3000
Wire Wire Line
	10850 2850 10850 3000
$Comp
L Device:C C?
U 1 1 6194B8A7
P 10850 2700
AR Path="/6194B8A7" Ref="C?"  Part="1" 
AR Path="/60C56809/6194B8A7" Ref="C?"  Part="1" 
AR Path="/6114C59C/6194B8A7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/6194B8A7" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6194B8A7" Ref="C?"  Part="1" 
AR Path="/60BA0495/6194B8A7" Ref="C23"  Part="1" 
F 0 "C23" H 10965 2746 50  0000 L CNN
F 1 "47uF" H 10965 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10888 2550 50  0001 C CNN
F 3 "~" H 10850 2700 50  0001 C CNN
	1    10850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 550  2350 550 
$Comp
L Connector:TestPoint TP?
U 1 1 61B04D4F
P 2500 550
AR Path="/61B04D4F" Ref="TP?"  Part="1" 
AR Path="/60C56809/61B04D4F" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61B04D4F" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61B04D4F" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61B04D4F" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61B04D4F" Ref="TP11"  Part="1" 
F 0 "TP11" V 2500 738 50  0000 L CNN
F 1 "TestPoint" H 2558 577 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2700 550 50  0001 C CNN
F 3 "~" H 2700 550 50  0001 C CNN
	1    2500 550 
	0    1    -1   0   
$EndComp
Text Label 2350 550  2    50   ~ 0
RESET_R
Wire Wire Line
	6250 6700 6100 6700
Wire Wire Line
	6250 6800 6100 6800
$Comp
L Connector:TestPoint TP?
U 1 1 6181665D
P 6250 6800
AR Path="/6181665D" Ref="TP?"  Part="1" 
AR Path="/60C56809/6181665D" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6181665D" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6181665D" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6181665D" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6181665D" Ref="TP26"  Part="1" 
F 0 "TP26" V 6250 6988 50  0000 L CNN
F 1 "TestPoint" H 6308 6827 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 6800 50  0001 C CNN
F 3 "~" H 6450 6800 50  0001 C CNN
	1    6250 6800
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61816663
P 6250 6700
AR Path="/61816663" Ref="TP?"  Part="1" 
AR Path="/60C56809/61816663" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61816663" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61816663" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61816663" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61816663" Ref="TP21"  Part="1" 
F 0 "TP21" V 6250 6888 50  0000 L CNN
F 1 "TestPoint" H 6308 6727 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 6700 50  0001 C CNN
F 3 "~" H 6450 6700 50  0001 C CNN
	1    6250 6700
	0    1    -1   0   
$EndComp
Text Label 6100 6700 2    50   ~ 0
USIM_CLK
Text Label 6100 6800 2    50   ~ 0
USIM_CLK
Text Label 1800 4100 0    50   ~ 0
GND_MCU
$Comp
L Device:R R?
U 1 1 61B88771
P 1750 3800
AR Path="/608DC206/61B88771" Ref="R?"  Part="1" 
AR Path="/60BA0495/61B88771" Ref="R?"  Part="1" 
F 0 "R?" H 1680 3754 50  0000 R CNN
F 1 "47k" H 1680 3845 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1680 3800 50  0001 C CNN
F 3 "~" H 1750 3800 50  0001 C CNN
F 4 "RS-03K1001FT" H 1750 3800 50  0001 C CNN "MPN"
F 5 "ST" H 1750 3800 50  0001 C CNN "FUNCTION"
	1    1750 3800
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61B88778
P 2100 3450
F 0 "Q?" H 2291 3496 50  0000 L CNN
F 1 "Q_NPN_BEC" H 2291 3405 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2300 3550 50  0001 C CNN
F 3 "~" H 2100 3450 50  0001 C CNN
F 4 "PMBT3904,215" H 2100 3450 50  0001 C CNN "MPN"
	1    2100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3450 1750 3650
$Comp
L Device:R R?
U 1 1 61B8877F
P 1450 3450
AR Path="/608DC206/61B8877F" Ref="R?"  Part="1" 
AR Path="/60BA0495/61B8877F" Ref="R?"  Part="1" 
F 0 "R?" H 1380 3404 50  0000 R CNN
F 1 "4.7k" H 1380 3495 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 3450 50  0001 C CNN
F 3 "~" H 1450 3450 50  0001 C CNN
	1    1450 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1750 3950 1750 4100
Wire Wire Line
	2200 3650 2200 4100
Wire Wire Line
	1750 3450 1900 3450
Wire Wire Line
	1600 3450 1750 3450
Connection ~ 1750 3450
Wire Wire Line
	1300 3450 1150 3450
Wire Wire Line
	1750 4100 2200 4100
Wire Wire Line
	2200 3250 2200 3100
Text Label 1750 5900 0    50   ~ 0
GND_MCU
$Comp
L Device:R R?
U 1 1 61BAA48A
P 1700 5600
AR Path="/608DC206/61BAA48A" Ref="R?"  Part="1" 
AR Path="/60BA0495/61BAA48A" Ref="R?"  Part="1" 
F 0 "R?" H 1630 5554 50  0000 R CNN
F 1 "47k" H 1630 5645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1630 5600 50  0001 C CNN
F 3 "~" H 1700 5600 50  0001 C CNN
F 4 "RS-03K1001FT" H 1700 5600 50  0001 C CNN "MPN"
F 5 "ST" H 1700 5600 50  0001 C CNN "FUNCTION"
	1    1700 5600
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61BAA491
P 2050 5250
F 0 "Q?" H 2241 5296 50  0000 L CNN
F 1 "Q_NPN_BEC" H 2241 5205 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2250 5350 50  0001 C CNN
F 3 "~" H 2050 5250 50  0001 C CNN
F 4 "PMBT3904,215" H 2050 5250 50  0001 C CNN "MPN"
	1    2050 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5250 1700 5450
$Comp
L Device:R R?
U 1 1 61BAA498
P 1400 5250
AR Path="/608DC206/61BAA498" Ref="R?"  Part="1" 
AR Path="/60BA0495/61BAA498" Ref="R?"  Part="1" 
F 0 "R?" H 1330 5204 50  0000 R CNN
F 1 "4.7k" H 1330 5295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1330 5250 50  0001 C CNN
F 3 "~" H 1400 5250 50  0001 C CNN
	1    1400 5250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 5750 1700 5900
Wire Wire Line
	2150 5450 2150 5900
Wire Wire Line
	1700 5250 1850 5250
Wire Wire Line
	1550 5250 1700 5250
Connection ~ 1700 5250
Wire Wire Line
	1250 5250 1100 5250
Wire Wire Line
	1700 5900 2150 5900
Wire Wire Line
	2150 5050 2150 4900
$EndSCHEMATC
