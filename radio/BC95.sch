EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "Quectel BC92"
Date "2021-04-21"
Rev "1"
Comp "WISE FARM"
Comment1 " NB_IoT  Module "
Comment2 "Vitor Canosa"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4300 1750 4300 1900
$Comp
L custom:BC95 U?
U 1 1 61D38EE4
P 4600 3250
AR Path="/61D38EE4" Ref="U?"  Part="1" 
AR Path="/60C56809/61D38EE4" Ref="U4"  Part="1" 
AR Path="/6114C59C/61D38EE4" Ref="U?"  Part="1" 
AR Path="/61CCC1A5/61D38EE4" Ref="U6"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EE4" Ref="U?"  Part="1" 
AR Path="/60BA0495/61D38EE4" Ref="U6"  Part="1" 
F 0 "U6" H 4600 3300 50  0000 C CNN
F 1 "BC95" H 4600 3200 50  0000 C CNN
F 2 "custom:BC95" H 5150 3250 50  0001 C CNN
F 3 "" H 5150 3250 50  0001 C CNN
	1    4600 3250
	1    0    0    -1  
$EndComp
Text Label 4200 1750 1    50   ~ 0
ANT_IMN
Wire Wire Line
	4200 1900 4200 1750
Wire Wire Line
	4400 1750 4400 1900
Wire Wire Line
	4700 1750 4700 1900
Wire Wire Line
	4800 1750 4800 1900
Wire Wire Line
	6000 2400 5850 2400
Wire Wire Line
	6000 2900 5850 2900
Wire Wire Line
	1300 2950 1150 2950
Wire Wire Line
	1650 2750 1650 2600
Text Label 1150 2950 2    50   ~ 0
NB_RESET
$Comp
L Device:C C?
U 1 1 61D38EE7
P 8750 2700
AR Path="/61D38EE7" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38EE7" Ref="C16"  Part="1" 
AR Path="/6114C59C/61D38EE7" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38EE7" Ref="C49"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EE7" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38EE7" Ref="C36"  Part="1" 
F 0 "C36" H 8865 2746 50  0000 L CNN
F 1 "100nF" H 8865 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8788 2550 50  0001 C CNN
F 3 "~" H 8750 2700 50  0001 C CNN
	1    8750 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61D38EE8
P 9200 2700
AR Path="/61D38EE8" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38EE8" Ref="C17"  Part="1" 
AR Path="/6114C59C/61D38EE8" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38EE8" Ref="C52"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EE8" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38EE8" Ref="C39"  Part="1" 
F 0 "C39" H 9315 2746 50  0000 L CNN
F 1 "100pF" H 9315 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9238 2550 50  0001 C CNN
F 3 "~" H 9200 2700 50  0001 C CNN
	1    9200 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2550 8750 2400
Wire Wire Line
	9200 2550 9200 2400
Wire Wire Line
	8750 2850 8750 3000
Wire Wire Line
	9200 2850 9200 3000
$Comp
L Device:C C?
U 1 1 61D38F00
P 9700 2700
AR Path="/61D38F00" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38F00" Ref="C20"  Part="1" 
AR Path="/6114C59C/61D38F00" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38F00" Ref="C54"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F00" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38F00" Ref="C41"  Part="1" 
F 0 "C41" H 9815 2746 50  0000 L CNN
F 1 "22pF" H 9815 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9738 2550 50  0001 C CNN
F 3 "~" H 9700 2700 50  0001 C CNN
	1    9700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2550 9700 2400
Wire Wire Line
	9700 2850 9700 3000
Wire Wire Line
	8750 3000 9200 3000
Wire Wire Line
	9200 3000 9700 3000
Connection ~ 9200 3000
Wire Wire Line
	8750 2400 9200 2400
Wire Wire Line
	9700 2400 9200 2400
Connection ~ 9200 2400
Text HLabel 1100 700  0    50   Input ~ 0
VCC_R
Text HLabel 1100 850  0    50   Input ~ 0
GND_R
Wire Wire Line
	1100 700  1250 700 
Wire Wire Line
	1100 850  1250 850 
Text Label 1250 700  0    50   ~ 0
VCC_R
Text Label 1250 850  0    50   ~ 0
GND_R
Text Label 4300 1750 1    50   ~ 0
GND_R
Text Label 4400 1750 1    50   ~ 0
GND_R
Text Label 4700 1750 1    50   ~ 0
GND_R
Text Label 4800 1750 1    50   ~ 0
GND_R
Text Label 6000 2400 0    50   ~ 0
GND_R
Text Label 6000 2500 0    50   ~ 0
GND_R
Text Label 8900 2400 0    50   ~ 0
VCC_R
Text Label 8900 3000 0    50   ~ 0
GND_R
Text Label 4900 1750 1    50   ~ 0
VCC_R
Text Label 5000 1750 1    50   ~ 0
VCC_R
Text Label 1650 3450 0    50   ~ 0
GND_R
Text HLabel 1100 1900 0    50   Input ~ 0
RESET_R
Wire Wire Line
	1100 1900 1250 1900
Text Label 1650 2600 0    50   ~ 0
RESET
Text Label 3200 3800 2    50   ~ 0
RESET
Text HLabel 1100 1150 0    50   Output ~ 0
TXD_R
Text HLabel 1100 1300 0    50   Input ~ 0
RXD_R
Wire Wire Line
	1100 1150 1250 1150
Wire Wire Line
	1100 1300 1250 1300
Text Label 1250 1900 0    50   ~ 0
NB_RESET
Text Label 1250 1150 0    50   ~ 0
NB_TXD
Text Label 1250 1300 0    50   ~ 0
NB_RXD
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 6132A8BF
P 1450 5050
AR Path="/6132A8BF" Ref="Q?"  Part="1" 
AR Path="/60C56809/6132A8BF" Ref="Q2"  Part="1" 
AR Path="/6114C59C/6132A8BF" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/6132A8BF" Ref="Q12"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8BF" Ref="Q?"  Part="1" 
AR Path="/60BA0495/6132A8BF" Ref="Q7"  Part="1" 
F 0 "Q7" H 1641 5096 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1641 5005 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1650 5150 50  0001 C CNN
F 3 "~" H 1450 5050 50  0001 C CNN
F 4 "PMBT3904,215" H 1450 5050 50  0001 C CNN "MPN"
	1    1450 5050
	0    -1   1    0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 6132A8C0
P 1750 6450
AR Path="/6132A8C0" Ref="Q?"  Part="1" 
AR Path="/60C56809/6132A8C0" Ref="Q1"  Part="1" 
AR Path="/6114C59C/6132A8C0" Ref="Q?"  Part="1" 
AR Path="/61CCC1A5/6132A8C0" Ref="Q11"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8C0" Ref="Q?"  Part="1" 
AR Path="/60BA0495/6132A8C0" Ref="Q8"  Part="1" 
F 0 "Q8" H 1941 6496 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1941 6405 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1950 6550 50  0001 C CNN
F 3 "~" H 1750 6450 50  0001 C CNN
F 4 "PMBT3904,215" H 1750 6450 50  0001 C CNN "MPN"
	1    1750 6450
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 6550 2250 6550
Wire Wire Line
	2250 6550 2250 6400
Wire Wire Line
	2250 6100 2250 5950
$Comp
L Device:R R?
U 1 1 60D08915
P 1750 5950
AR Path="/60D08915" Ref="R?"  Part="1" 
AR Path="/60C56809/60D08915" Ref="R13"  Part="1" 
AR Path="/6114C59C/60D08915" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60D08915" Ref="R40"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60D08915" Ref="R?"  Part="1" 
AR Path="/60BA0495/60D08915" Ref="R30"  Part="1" 
F 0 "R30" H 1680 5904 50  0000 R CNN
F 1 "4.7k" H 1680 5995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1680 5950 50  0001 C CNN
F 3 "~" H 1750 5950 50  0001 C CNN
	1    1750 5950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 5800 1750 5650
Wire Wire Line
	1750 6250 1750 6100
Wire Wire Line
	2250 6550 2400 6550
Connection ~ 2250 6550
Text Label 2400 6550 0    50   ~ 0
NB_TXD
Text HLabel 1100 1600 0    50   Input ~ 0
VCC_BATT_R
Text HLabel 1100 1450 0    50   Input ~ 0
BJT_CTRL_R
Wire Wire Line
	1100 1600 1250 1600
Wire Wire Line
	1100 1450 1250 1450
Text Label 1250 1600 0    50   ~ 0
NB_VCC_BATT
Text Label 1250 1450 0    50   ~ 0
BJT_CTRL
Text Label 1750 5650 0    50   ~ 0
BJT_CTRL
Wire Wire Line
	5850 3700 6000 3700
Text Label 6000 3700 0    50   ~ 0
TXD
Text Label 750  6550 2    50   ~ 0
TXD
$Comp
L Device:R R?
U 1 1 60D49A08
P 1450 4550
AR Path="/60D49A08" Ref="R?"  Part="1" 
AR Path="/60C56809/60D49A08" Ref="R15"  Part="1" 
AR Path="/6114C59C/60D49A08" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60D49A08" Ref="R41"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60D49A08" Ref="R?"  Part="1" 
AR Path="/60BA0495/60D49A08" Ref="R29"  Part="1" 
F 0 "R29" H 1380 4504 50  0000 R CNN
F 1 "4.7k" H 1380 4595 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1380 4550 50  0001 C CNN
F 3 "~" H 1450 4550 50  0001 C CNN
	1    1450 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 5150 950  5150
Wire Wire Line
	950  5150 950  5050
Wire Wire Line
	950  4750 950  4600
Wire Wire Line
	1450 4400 1450 4250
Wire Wire Line
	1450 4700 1450 4850
Wire Wire Line
	950  5150 800  5150
Connection ~ 950  5150
Wire Wire Line
	1650 5150 1800 5150
Wire Wire Line
	4950 4600 4950 4750
Text Label 4950 4750 3    50   ~ 0
VDD_EXT
Text Label 950  4600 2    50   ~ 0
VDD_EXT
Text Label 1800 5150 0    50   ~ 0
NB_RXD
Wire Wire Line
	5850 3800 6000 3800
Text Label 6000 3800 0    50   ~ 0
RXD
Text Label 800  5150 2    50   ~ 0
RXD
Text Notes 1750 5300 0    50   ~ 0
(MCU_TXD)
Text Notes 2350 6700 0    50   ~ 0
(MCU_RXD)
Text Label 6000 2800 0    50   ~ 0
USIM_RST
Wire Wire Line
	5850 2700 6000 2700
Wire Wire Line
	5850 2600 6000 2600
Wire Wire Line
	5850 2500 6000 2500
Wire Wire Line
	5850 2800 6000 2800
Text Label 6000 2900 0    50   ~ 0
USIM_VCC
Text Label 6000 2600 0    50   ~ 0
USIM_CLK
Text Label 6000 2700 0    50   ~ 0
USIM_DATA
Wire Wire Line
	4100 1900 4100 1750
Text Label 4100 1750 1    50   ~ 0
GND_R
NoConn ~ 3400 2600
Text Notes 7850 2250 0    50   ~ 0
The input voltage of VBAT ranges from 3.4V to 4.2V.\nThe module drains a maximum current of approx. 1.6A during GSM burst.\nThe minimum width of VBAT trace is recommended to be 2mm.
$Comp
L Device:C C?
U 1 1 60ED0266
P 4650 6350
AR Path="/60ED0266" Ref="C?"  Part="1" 
AR Path="/60C56809/60ED0266" Ref="C10"  Part="1" 
AR Path="/6114C59C/60ED0266" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60ED0266" Ref="C43"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60ED0266" Ref="C?"  Part="1" 
AR Path="/60BA0495/60ED0266" Ref="C30"  Part="1" 
F 0 "C30" H 4765 6396 50  0000 L CNN
F 1 "4.7uF" H 4765 6305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4688 6200 50  0001 C CNN
F 3 "~" H 4650 6350 50  0001 C CNN
	1    4650 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6500 4650 6650
Wire Wire Line
	4650 6200 4650 6050
Text Label 4650 6650 0    50   ~ 0
GND_R
Text Label 4650 6050 0    50   ~ 0
VDD_EXT
Text Notes 4050 6050 0    50   ~ 0
It is recommended to use VDD_EXT for \nthe external pull-up power supply for I/O\nUsed for external pull up circuits, and\nneed to add a 2.2~4.7uF bypass\ncapacitor in\nparallel.
Text Notes 7750 3350 0    50   ~ 0
These capacitor are arranged with capacitances in ascending order. \nCapacitor with lowest capacitance is closest to VBAT pin and all \ncapacitors should be as close to VBAT pin as possible.\n
Text Notes 1200 2450 0    50   ~ 0
Reset Reference Circuit
Text Notes 650  4050 0    50   ~ 0
Level Translation - Transistor Solution
Text Notes 600  7600 0    50   ~ 0
The transistor circuit solution is not suitable for applications with high baud rates exceeding 460Kbps\nIn order to ensure the effective operation of the transistor circuit, BJT_CTL needs to select the lower of the two levels\nWhen MCU_VDD<VDD_EXT, BJT_CTL=MCU_VDD. (If low-power design is required, a MCU_IO\nshould be selected as BJT_CTL. When the module enters deep sleep, MCU controls MCU_IO as low level.\nIf high baud rate is needed, it is highly recommended to install two 1nF capacitors on transistor circiuts.
Text HLabel 1100 1750 0    50   Output ~ 0
UCR_R
Wire Wire Line
	1100 1750 1250 1750
Text Label 1250 1750 0    50   ~ 0
NB_URC
Wire Wire Line
	5850 3300 6000 3300
Wire Wire Line
	2550 750  2400 750 
Wire Wire Line
	2550 850  2400 850 
$Comp
L Connector:TestPoint TP?
U 1 1 61D38F08
P 2550 850
AR Path="/61D38F08" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38F08" Ref="TP9"  Part="1" 
AR Path="/6114C59C/61D38F08" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38F08" Ref="TP29"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F08" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38F08" Ref="TP24"  Part="1" 
F 0 "TP24" V 2550 1038 50  0000 L CNN
F 1 "TestPoint" H 2608 877 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 850 50  0001 C CNN
F 3 "~" H 2750 850 50  0001 C CNN
	1    2550 850 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61D38F09
P 2550 750
AR Path="/61D38F09" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38F09" Ref="TP2"  Part="1" 
AR Path="/6114C59C/61D38F09" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38F09" Ref="TP28"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F09" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38F09" Ref="TP23"  Part="1" 
F 0 "TP23" V 2550 938 50  0000 L CNN
F 1 "TestPoint" H 2608 777 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 750 50  0001 C CNN
F 3 "~" H 2750 750 50  0001 C CNN
	1    2550 750 
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8DA
P 2550 950
AR Path="/6132A8DA" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8DA" Ref="TP10"  Part="1" 
AR Path="/6114C59C/6132A8DA" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6132A8DA" Ref="TP30"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8DA" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6132A8DA" Ref="TP25"  Part="1" 
F 0 "TP25" V 2550 1138 50  0000 L CNN
F 1 "TestPoint" H 2608 977 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 950 50  0001 C CNN
F 3 "~" H 2750 950 50  0001 C CNN
	1    2550 950 
	0    1    -1   0   
$EndComp
Wire Wire Line
	2550 950  2400 950 
Text Label 2400 750  2    50   ~ 0
NB_RESET
Text Label 2400 850  2    50   ~ 0
NB_TXD
Text Label 2400 950  2    50   ~ 0
NB_RXD
Text Notes 800  1050 0    50   ~ 0
(MCU_TXD / MCU_RXD)
Wire Wire Line
	2550 1250 2400 1250
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8DF
P 2550 1250
AR Path="/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8DF" Ref="TP13"  Part="1" 
AR Path="/6114C59C/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6132A8DF" Ref="TP33"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8DF" Ref="TP?"  Part="1" 
AR Path="/60BA0495/6132A8DF" Ref="TP32"  Part="1" 
F 0 "TP32" V 2550 1438 50  0000 L CNN
F 1 "TestPoint" H 2608 1277 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 1250 50  0001 C CNN
F 3 "~" H 2750 1250 50  0001 C CNN
	1    2550 1250
	0    1    -1   0   
$EndComp
Wire Wire Line
	2550 1450 2400 1450
Wire Wire Line
	2550 1550 2400 1550
$Comp
L Connector:TestPoint TP?
U 1 1 61D38EFD
P 2550 1550
AR Path="/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38EFD" Ref="TP16"  Part="1" 
AR Path="/6114C59C/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38EFD" Ref="TP36"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EFD" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38EFD" Ref="TP46"  Part="1" 
F 0 "TP46" V 2550 1738 50  0000 L CNN
F 1 "TestPoint" H 2608 1577 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 1550 50  0001 C CNN
F 3 "~" H 2750 1550 50  0001 C CNN
	1    2550 1550
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61D38EFE
P 2550 1450
AR Path="/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/60C56809/61D38EFE" Ref="TP15"  Part="1" 
AR Path="/6114C59C/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61D38EFE" Ref="TP35"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38EFE" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61D38EFE" Ref="TP45"  Part="1" 
F 0 "TP45" V 2550 1638 50  0000 L CNN
F 1 "TestPoint" H 2608 1477 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2750 1450 50  0001 C CNN
F 3 "~" H 2750 1450 50  0001 C CNN
	1    2550 1450
	0    1    -1   0   
$EndComp
$Comp
L custom:MSQA6V1W5T2G D5
U 1 1 6132A8E4
P 8900 6150
AR Path="/60C56809/6132A8E4" Ref="D5"  Part="1" 
AR Path="/6114C59C/6132A8E4" Ref="D?"  Part="1" 
AR Path="/61CCC1A5/6132A8E4" Ref="D10"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6132A8E4" Ref="D?"  Part="1" 
AR Path="/60BA0495/6132A8E4" Ref="D4"  Part="1" 
F 0 "D4" H 8850 6537 60  0000 C CNN
F 1 "MSQA6V1W5T2G" H 8850 6431 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5" H 9100 6350 60  0001 L CNN
F 3 "" H 9100 6450 60  0001 L CNN
	1    8900 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 6050 8250 6050
Wire Wire Line
	8400 6150 8250 6150
Wire Wire Line
	8250 6250 8400 6250
Wire Wire Line
	9300 6050 9450 6050
Wire Wire Line
	9300 6250 9450 6250
Text Label 9450 6050 0    50   ~ 0
USIM_VDD
Text Label 8250 6150 2    50   ~ 0
GND_R
Text Label 8250 6050 2    50   ~ 0
USIM_DATA_TVS
Text Label 8250 6250 2    50   ~ 0
USIM_CLK_TVS
Text Label 9450 6250 0    50   ~ 0
USIM_RST_TVS
$Comp
L Device:R R?
U 1 1 61D38F0F
P 2250 6250
AR Path="/61D38F0F" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38F0F" Ref="R16"  Part="1" 
AR Path="/6114C59C/61D38F0F" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38F0F" Ref="R53"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F0F" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38F0F" Ref="R32"  Part="1" 
F 0 "R32" H 2180 6204 50  0000 R CNN
F 1 "10k" H 2180 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2180 6250 50  0001 C CNN
F 3 "~" H 2250 6250 50  0001 C CNN
	1    2250 6250
	-1   0    0    -1  
$EndComp
Text Label 6000 3300 0    50   ~ 0
NB_URC
$Comp
L Device:C C?
U 1 1 61D38F11
P 9100 5150
AR Path="/61D38F11" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38F11" Ref="C22"  Part="1" 
AR Path="/6114C59C/61D38F11" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38F11" Ref="C50"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F11" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38F11" Ref="C37"  Part="1" 
F 0 "C37" H 9215 5196 50  0000 L CNN
F 1 "33pF" H 9215 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9138 5000 50  0001 C CNN
F 3 "~" H 9100 5150 50  0001 C CNN
	1    9100 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61D38F12
P 8650 5150
AR Path="/61D38F12" Ref="C?"  Part="1" 
AR Path="/60C56809/61D38F12" Ref="C21"  Part="1" 
AR Path="/6114C59C/61D38F12" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61D38F12" Ref="C47"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F12" Ref="C?"  Part="1" 
AR Path="/60BA0495/61D38F12" Ref="C34"  Part="1" 
F 0 "C34" H 8765 5196 50  0000 L CNN
F 1 "33pF" H 8765 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8688 5000 50  0001 C CNN
F 3 "~" H 8650 5150 50  0001 C CNN
	1    8650 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 619425EA
P 8250 5150
AR Path="/619425EA" Ref="C?"  Part="1" 
AR Path="/60C56809/619425EA" Ref="C19"  Part="1" 
AR Path="/6114C59C/619425EA" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/619425EA" Ref="C44"  Part="1" 
AR Path="/6083CF6C/6083E0CF/619425EA" Ref="C?"  Part="1" 
AR Path="/60BA0495/619425EA" Ref="C31"  Part="1" 
F 0 "C31" H 8365 5196 50  0000 L CNN
F 1 "33pF" H 8365 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8288 5000 50  0001 C CNN
F 3 "~" H 8250 5150 50  0001 C CNN
	1    8250 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 5300 8250 5450
Wire Wire Line
	8250 5450 8650 5450
Wire Wire Line
	9100 5450 9100 5300
Wire Wire Line
	8650 5300 8650 5450
Connection ~ 8650 5450
Wire Wire Line
	8650 5450 9100 5450
Wire Wire Line
	9100 5000 9100 4650
Wire Wire Line
	8650 4350 8650 5000
Wire Wire Line
	8250 4250 8250 5000
$Comp
L Device:R R?
U 1 1 619425F9
P 7950 4250
AR Path="/619425F9" Ref="R?"  Part="1" 
AR Path="/60C56809/619425F9" Ref="R26"  Part="1" 
AR Path="/6114C59C/619425F9" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/619425F9" Ref="R63"  Part="1" 
AR Path="/6083CF6C/6083E0CF/619425F9" Ref="R?"  Part="1" 
AR Path="/60BA0495/619425F9" Ref="R34"  Part="1" 
F 0 "R34" H 7880 4204 50  0000 R CNN
F 1 "22R" H 7880 4295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7880 4250 50  0001 C CNN
F 3 "~" H 7950 4250 50  0001 C CNN
	1    7950 4250
	0    -1   1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61D38F15
P 7500 4350
AR Path="/61D38F15" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38F15" Ref="R25"  Part="1" 
AR Path="/6114C59C/61D38F15" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38F15" Ref="R62"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F15" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38F15" Ref="R33"  Part="1" 
F 0 "R33" H 7430 4304 50  0000 R CNN
F 1 "22R" H 7430 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7430 4350 50  0001 C CNN
F 3 "~" H 7500 4350 50  0001 C CNN
	1    7500 4350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 61942606
P 7950 4650
AR Path="/61942606" Ref="R?"  Part="1" 
AR Path="/60C56809/61942606" Ref="R27"  Part="1" 
AR Path="/6114C59C/61942606" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61942606" Ref="R64"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61942606" Ref="R?"  Part="1" 
AR Path="/60BA0495/61942606" Ref="R35"  Part="1" 
F 0 "R35" H 7880 4604 50  0000 R CNN
F 1 "22R" H 7880 4695 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7880 4650 50  0001 C CNN
F 3 "~" H 7950 4650 50  0001 C CNN
	1    7950 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7800 4250 7200 4250
Wire Wire Line
	7350 4350 7200 4350
Wire Wire Line
	7800 4650 7200 4650
$Comp
L Device:R R?
U 1 1 61D38F17
P 8900 4150
AR Path="/61D38F17" Ref="R?"  Part="1" 
AR Path="/60C56809/61D38F17" Ref="R28"  Part="1" 
AR Path="/6114C59C/61D38F17" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/61D38F17" Ref="R65"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F17" Ref="R?"  Part="1" 
AR Path="/60BA0495/61D38F17" Ref="R37"  Part="1" 
F 0 "R37" H 8830 4104 50  0000 R CNN
F 1 "10k" H 8830 4195 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 4150 50  0001 C CNN
F 3 "~" H 8900 4150 50  0001 C CNN
	1    8900 4150
	0    -1   1    0   
$EndComp
Wire Wire Line
	8750 4150 8600 4150
$Comp
L Device:C C?
U 1 1 61942619
P 9500 5150
AR Path="/61942619" Ref="C?"  Part="1" 
AR Path="/60C56809/61942619" Ref="C23"  Part="1" 
AR Path="/6114C59C/61942619" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/61942619" Ref="C53"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61942619" Ref="C?"  Part="1" 
AR Path="/60BA0495/61942619" Ref="C40"  Part="1" 
F 0 "C40" H 9615 5196 50  0000 L CNN
F 1 "100nF" H 9615 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9538 5000 50  0001 C CNN
F 3 "~" H 9500 5150 50  0001 C CNN
	1    9500 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 5300 9500 5450
Text Label 9150 4450 2    50   ~ 0
GND_R
NoConn ~ 9550 4550
Text Label 8550 5450 2    50   ~ 0
GND_R
Text Label 9550 4250 2    50   ~ 0
USIM_RST_TVS
Text Label 8600 4350 2    50   ~ 0
USIM_CLK_TVS
Text Label 8700 4650 2    50   ~ 0
USIM_DATA_TVS
Connection ~ 8250 4250
Wire Wire Line
	8250 4250 8100 4250
Connection ~ 8650 4350
Connection ~ 9100 4650
Wire Wire Line
	9100 4650 9550 4650
Wire Wire Line
	8250 4250 9550 4250
Wire Wire Line
	8650 4350 9550 4350
Wire Wire Line
	9500 4150 9500 5000
Connection ~ 9500 4150
Wire Wire Line
	9500 4150 9550 4150
Wire Wire Line
	9050 4150 9400 4150
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6194263A
P 9400 4000
AR Path="/6114C59C/6194263A" Ref="#FLG?"  Part="1" 
AR Path="/60C56809/6194263A" Ref="#FLG0105"  Part="1" 
AR Path="/61CCC1A5/6194263A" Ref="#FLG0101"  Part="1" 
AR Path="/6083CF6C/6083E0CF/6194263A" Ref="#FLG?"  Part="1" 
AR Path="/60BA0495/6194263A" Ref="#FLG04"  Part="1" 
F 0 "#FLG04" H 9400 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 9400 4173 50  0000 C CNN
F 2 "" H 9400 4000 50  0001 C CNN
F 3 "~" H 9400 4000 50  0001 C CNN
	1    9400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4000 9400 4150
Connection ~ 9400 4150
Wire Wire Line
	9400 4150 9500 4150
Wire Wire Line
	9150 4450 9550 4450
Text Notes 8150 3850 0    50   ~ 0
USIM Interface
Text Label 3750 4750 3    50   ~ 0
GND_R
Wire Wire Line
	4150 4600 4150 4750
Wire Wire Line
	3750 4600 3750 4750
Text Label 3850 4750 3    50   ~ 0
GND_R
Wire Wire Line
	3850 4600 3850 4750
Text Label 3950 4750 3    50   ~ 0
GND_R
Wire Wire Line
	3950 4600 3950 4750
Text Label 5150 4750 3    50   ~ 0
GND_R
Wire Wire Line
	5150 4600 5150 4750
Text Label 5250 4750 3    50   ~ 0
GND_R
Wire Wire Line
	5250 4600 5250 4750
Text Label 5350 4750 3    50   ~ 0
GND_R
Wire Wire Line
	5350 4600 5350 4750
Text Label 5450 4750 3    50   ~ 0
GND_R
Wire Wire Line
	5450 4600 5450 4750
Wire Wire Line
	4000 1900 4000 1750
Text Label 4000 1750 1    50   ~ 0
GND_R
Wire Wire Line
	3900 1900 3900 1750
Text Label 3900 1750 1    50   ~ 0
GND_R
Wire Wire Line
	3800 1900 3800 1750
Text Label 3800 1750 1    50   ~ 0
GND_R
Wire Wire Line
	4900 1900 4900 1750
Wire Wire Line
	5000 1900 5000 1750
Wire Wire Line
	5200 1750 5200 1900
Text Label 5200 1750 1    50   ~ 0
GND_R
Wire Wire Line
	5300 1750 5300 1900
Text Label 5300 1750 1    50   ~ 0
GND_R
Wire Wire Line
	5400 1750 5400 1900
Text Label 5400 1750 1    50   ~ 0
GND_R
Wire Wire Line
	5500 1750 5500 1900
Text Label 5500 1750 1    50   ~ 0
GND_R
Text Label 3200 2400 2    50   ~ 0
GND_R
Text Label 3200 4100 2    50   ~ 0
GND_R
Wire Wire Line
	6000 4100 5850 4100
Text Label 6000 4100 0    50   ~ 0
GND_R
Wire Wire Line
	9100 5450 9500 5450
Connection ~ 9100 5450
Text Label 3200 4000 2    50   ~ 0
GND_R
Wire Wire Line
	6000 4000 5850 4000
Text Label 6000 4000 0    50   ~ 0
GND_R
$Comp
L dk_Transistors-Bipolar-BJT-Single-Pre-Biased:MMUN2211LT1G Q10
U 1 1 60C0D09C
P 1650 2950
F 0 "Q10" H 1738 3003 60  0000 L CNN
F 1 "RN1311" H 1738 2897 60  0000 L CNN
F 2 "digikey-footprints:SOT-323" H 1850 3150 60  0001 L CNN
F 3 "" H 1850 3250 60  0001 L CNN
F 4 "" H 1850 3350 60  0001 L CNN "Digi-Key_PN"
F 5 "RN1311" H 1850 3450 60  0001 L CNN "MPN"
F 6 "" H 1850 3550 60  0001 L CNN "Category"
F 7 "" H 1850 3650 60  0001 L CNN "Family"
F 8 "" H 1850 3750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "" H 1850 3850 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS PREBIAS NPN 246MW SOT23-3" H 1850 3950 60  0001 L CNN "Description"
F 11 "" H 1850 4050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1850 4150 60  0001 L CNN "Status"
	1    1650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3150 1650 3450
Wire Wire Line
	900  6100 900  5950
Text Label 900  5950 2    50   ~ 0
VDD_EXT
Wire Wire Line
	900  6550 1550 6550
Wire Wire Line
	900  6550 900  6400
Wire Wire Line
	900  6550 750  6550
Connection ~ 900  6550
Text Notes 700  7000 0    50   ~ 0
In PSM, MCU_TXD cannot be left floating or the current consumption of BC95-G\n in PSM will increase. It is recommended to pull up the module’s RXD to VDD_EXT\n with a 2MΩ resistor for low current leakage.
Text Notes 3500 1000 0    50   ~ 0
BC_95\nVmax=4.2V\nVmin=3.1V\nVnorm=3.6V
Wire Wire Line
	4450 4600 4450 4750
Wire Wire Line
	3200 3800 3350 3800
Wire Wire Line
	3200 4000 3350 4000
Wire Wire Line
	3200 4100 3350 4100
Wire Wire Line
	3200 2400 3350 2400
Text Label 3200 2500 2    50   ~ 0
GND_R
Wire Wire Line
	3200 2500 3350 2500
NoConn ~ 4150 4750
Wire Wire Line
	9400 1150 9550 1150
Text Label 9400 1150 0    60   ~ 0
ANT
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 60FAFA59
P 8800 1450
AR Path="/60FAFA59" Ref="C?"  Part="1" 
AR Path="/60C56809/60FAFA59" Ref="C?"  Part="1" 
AR Path="/6114C59C/60FAFA59" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60FAFA59" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60FAFA59" Ref="C?"  Part="1" 
AR Path="/60BA0495/60FAFA59" Ref="C4"  Part="1" 
F 0 "C4" H 8900 1450 50  0000 L CNN
F 1 "X pF" H 8825 1350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8838 1300 30  0001 C CNN
F 3 "Kit smd capacitors" H 8800 1450 60  0001 C CNN
F 4 "---" H 8800 1450 60  0001 C CNN "digikey"
	1    8800 1450
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_2-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue C?
U 1 1 60FAFA58
P 9400 1450
AR Path="/60FAFA58" Ref="C?"  Part="1" 
AR Path="/60C56809/60FAFA58" Ref="C?"  Part="1" 
AR Path="/6114C59C/60FAFA58" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/60FAFA58" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60FAFA58" Ref="C?"  Part="1" 
AR Path="/60BA0495/60FAFA58" Ref="C8"  Part="1" 
F 0 "C8" H 9150 1450 50  0000 L CNN
F 1 "X pF" H 9050 1350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9438 1300 30  0001 C CNN
F 3 "Kit smd capacitors" H 9400 1450 60  0001 C CNN
F 4 "---" H 9400 1450 60  0001 C CNN "digikey"
	1    9400 1450
	-1   0    0    -1  
$EndComp
Text Notes 9500 900  2    60   ~ 0
PI Macthing Network
$Comp
L wise_farm_nb_iot_2-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue-wise_farm_nb_iot_1-rescue L?
U 1 1 61D38EEA
P 9100 1150
AR Path="/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60C56809/61D38EEA" Ref="L?"  Part="1" 
AR Path="/6114C59C/61D38EEA" Ref="L?"  Part="1" 
AR Path="/61CCC1A5/61D38EEA" Ref="L?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61D38EEA" Ref="L?"  Part="1" 
AR Path="/60BA0495/61D38EEA" Ref="L2"  Part="1" 
F 0 "L2" V 9050 1150 50  0000 C CNN
F 1 "L" V 9175 1150 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 9100 1150 50  0001 C CNN
F 3 "Kit smd inductors" H 9100 1150 50  0001 C CNN
F 4 "---" V 9100 1150 60  0001 C CNN "digikey"
	1    9100 1150
	0    1    -1   0   
$EndComp
Text Label 8650 1150 2    50   ~ 0
ANT_IMN
Wire Wire Line
	9400 1600 9400 1750
Wire Wire Line
	8800 1600 8800 1750
Wire Wire Line
	8800 1300 8800 1150
Wire Wire Line
	8800 1150 8950 1150
Wire Wire Line
	9400 1300 9400 1150
Wire Wire Line
	9400 1150 9250 1150
Connection ~ 9400 1150
Text Label 8800 1750 0    50   ~ 0
GND_R
Text Label 9400 1750 0    50   ~ 0
GND_R
Wire Wire Line
	8800 1150 8650 1150
Connection ~ 8800 1150
$Comp
L custom:ANT_CUSTOM U?
U 1 1 609AB7B3
P 9950 1150
AR Path="/60B4409A/609AB7B3" Ref="U?"  Part="1" 
AR Path="/60C56809/609AB7B3" Ref="U?"  Part="1" 
AR Path="/60BA0495/609AB7B3" Ref="U4"  Part="1" 
F 0 "U4" H 9969 1285 50  0000 L CNN
F 1 "ANT_CUSTOM" H 9950 1250 50  0001 C CNN
F 2 "custom:antenna_NB_IoT_B28_v1" H 9950 1150 50  0001 C CNN
F 3 "" H 9950 1150 50  0001 C CNN
	1    9950 1150
	1    0    0    -1  
$EndComp
Text Label 2400 1550 2    50   ~ 0
USIM_VCC
Text Label 2400 1450 2    50   ~ 0
USIM_DATA
Text Label 2400 1250 2    50   ~ 0
USIM_RST
Text Label 9450 4150 2    50   ~ 0
USIM_VCC
Text Label 7200 4350 2    50   ~ 0
USIM_CLK
Text Label 7200 4650 2    50   ~ 0
USIM_DATA
Text Label 7200 4250 2    50   ~ 0
USIM_RST
$Comp
L Connector:SIM_Card J?
U 1 1 61D38F10
P 10050 4450
AR Path="/61D38F10" Ref="J?"  Part="1" 
AR Path="/60C56809/61D38F10" Ref="J1"  Part="1" 
AR Path="/6114C59C/61D38F10" Ref="J?"  Part="1" 
AR Path="/61CCC1A5/61D38F10" Ref="J3"  Part="1" 
AR Path="/6083CF6C/6083E0CF/61D38F10" Ref="J?"  Part="1" 
AR Path="/60BA0495/61D38F10" Ref="J3"  Part="1" 
F 0 "J3" H 10680 4550 50  0000 L CNN
F 1 "SIM_Card" H 10680 4459 50  0000 L CNN
F 2 "custom:SNO-1305-B" H 10050 4800 50  0001 C CNN
F 3 " ~" H 10000 4450 50  0001 C CNN
	1    10050 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4350 8650 4350
Wire Wire Line
	8100 4650 9100 4650
Text Label 8600 4150 2    50   ~ 0
USIM_DATA_TVS
Text Label 1450 4250 0    50   ~ 0
BJT_CTRL
$Comp
L Device:C C?
U 1 1 611C5B6A
P 10150 2700
AR Path="/611C5B6A" Ref="C?"  Part="1" 
AR Path="/60C56809/611C5B6A" Ref="C?"  Part="1" 
AR Path="/6114C59C/611C5B6A" Ref="C?"  Part="1" 
AR Path="/61CCC1A5/611C5B6A" Ref="C?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/611C5B6A" Ref="C?"  Part="1" 
AR Path="/60BA0495/611C5B6A" Ref="C12"  Part="1" 
F 0 "C12" H 10265 2746 50  0000 L CNN
F 1 "47uF" H 10265 2655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10188 2550 50  0001 C CNN
F 3 "~" H 10150 2700 50  0001 C CNN
	1    10150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 2550 10150 2400
Wire Wire Line
	10150 2850 10150 3000
Wire Wire Line
	9700 2400 10150 2400
Connection ~ 9700 2400
Wire Wire Line
	10150 3000 9700 3000
Connection ~ 9700 3000
$Comp
L Connector:TestPoint TP?
U 1 1 61225260
P 6400 7450
AR Path="/61225260" Ref="TP?"  Part="1" 
AR Path="/60C56809/61225260" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61225260" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61225260" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61225260" Ref="TP?"  Part="1" 
AR Path="/608585CB/61225260" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61225260" Ref="TP14"  Part="1" 
F 0 "TP14" V 6400 7638 50  0000 L CNN
F 1 "TestPoint" H 6458 7477 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6600 7450 50  0001 C CNN
F 3 "~" H 6600 7450 50  0001 C CNN
	1    6400 7450
	0    1    -1   0   
$EndComp
Wire Wire Line
	6400 7450 6250 7450
Wire Wire Line
	6400 7550 6250 7550
$Comp
L Connector:TestPoint TP?
U 1 1 61225269
P 6400 7550
AR Path="/61225269" Ref="TP?"  Part="1" 
AR Path="/60C56809/61225269" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61225269" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61225269" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61225269" Ref="TP?"  Part="1" 
AR Path="/608585CB/61225269" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61225269" Ref="TP15"  Part="1" 
F 0 "TP15" V 6400 7738 50  0000 L CNN
F 1 "TestPoint" H 6458 7577 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6600 7550 50  0001 C CNN
F 3 "~" H 6600 7550 50  0001 C CNN
	1    6400 7550
	0    1    -1   0   
$EndComp
Text Label 6250 7450 2    50   ~ 0
VDD_EXT
Text Label 6250 7550 2    50   ~ 0
VDD_EXT
$Comp
L Connector:TestPoint TP?
U 1 1 61246D10
P 6400 7150
AR Path="/61246D10" Ref="TP?"  Part="1" 
AR Path="/60C56809/61246D10" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61246D10" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61246D10" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61246D10" Ref="TP?"  Part="1" 
AR Path="/608585CB/61246D10" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61246D10" Ref="TP30"  Part="1" 
F 0 "TP30" V 6400 7338 50  0000 L CNN
F 1 "TestPoint" H 6458 7177 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6600 7150 50  0001 C CNN
F 3 "~" H 6600 7150 50  0001 C CNN
	1    6400 7150
	0    1    -1   0   
$EndComp
Wire Wire Line
	6400 7150 6250 7150
Wire Wire Line
	6400 7250 6250 7250
$Comp
L Connector:TestPoint TP?
U 1 1 61246D18
P 6400 7250
AR Path="/61246D18" Ref="TP?"  Part="1" 
AR Path="/60C56809/61246D18" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61246D18" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61246D18" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61246D18" Ref="TP?"  Part="1" 
AR Path="/608585CB/61246D18" Ref="TP?"  Part="1" 
AR Path="/60BA0495/61246D18" Ref="TP31"  Part="1" 
F 0 "TP31" V 6400 7438 50  0000 L CNN
F 1 "TestPoint" H 6458 7277 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6600 7250 50  0001 C CNN
F 3 "~" H 6600 7250 50  0001 C CNN
	1    6400 7250
	0    1    -1   0   
$EndComp
Text Label 6250 7250 2    50   ~ 0
USIM_DATA_TVS
Text Label 6250 7150 2    50   ~ 0
USIM_DATA_TVS
Text HLabel 1100 2050 0    50   Input ~ 0
VCC_MCU_R
Wire Wire Line
	1100 2050 1250 2050
Text Label 1250 2050 0    50   ~ 0
VCC_MCU_R
Text Label 2250 5950 0    50   ~ 0
VCC_MCU_R
$Comp
L Device:R R?
U 1 1 60C335DE
P 900 6250
AR Path="/60C335DE" Ref="R?"  Part="1" 
AR Path="/60C56809/60C335DE" Ref="R?"  Part="1" 
AR Path="/6114C59C/60C335DE" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60C335DE" Ref="R?"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60C335DE" Ref="R?"  Part="1" 
AR Path="/60BA0495/60C335DE" Ref="R16"  Part="1" 
F 0 "R16" H 830 6204 50  0000 R CNN
F 1 "2M" H 830 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 830 6250 50  0001 C CNN
F 3 "~" H 900 6250 50  0001 C CNN
	1    900  6250
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60D49F6A
P 950 4900
AR Path="/60D49F6A" Ref="R?"  Part="1" 
AR Path="/60C56809/60D49F6A" Ref="R7"  Part="1" 
AR Path="/6114C59C/60D49F6A" Ref="R?"  Part="1" 
AR Path="/61CCC1A5/60D49F6A" Ref="R38"  Part="1" 
AR Path="/6083CF6C/6083E0CF/60D49F6A" Ref="R?"  Part="1" 
AR Path="/60BA0495/60D49F6A" Ref="R11"  Part="1" 
F 0 "R11" H 880 4854 50  0000 R CNN
F 1 "10k" H 880 4945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 880 4900 50  0001 C CNN
F 3 "~" H 950 4900 50  0001 C CNN
	1    950  4900
	-1   0    0    -1  
$EndComp
NoConn ~ 4450 4750
$EndSCHEMATC
