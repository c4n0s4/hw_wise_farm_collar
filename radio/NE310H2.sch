EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_1-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Quectel NE310H2"
Date "2021-04-21"
Rev "1"
Comp "WISE FARM"
Comment1 " NB_IoT  Module NE310H2"
Comment2 "Vitor Canosa"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6250 1850 6100 1850
Wire Wire Line
	6250 1950 6100 1950
Wire Wire Line
	2750 3550 2900 3550
$Comp
L Device:R R?
U 1 1 60C6A488
P 3600 7400
AR Path="/60C6A488" Ref="R?"  Part="1" 
AR Path="/60C56809/60C6A488" Ref="R20"  Part="1" 
AR Path="/6114C59C/60C6A488" Ref="R37"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60C6A488" Ref="R?"  Part="1" 
F 0 "R?" H 3530 7354 50  0000 R CNN
F 1 "47k" H 3530 7445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3530 7400 50  0001 C CNN
F 3 "~" H 3600 7400 50  0001 C CNN
F 4 "RS-03K1001FT" H 3600 7400 50  0001 C CNN "MPN"
F 5 "ST" H 3600 7400 50  0001 C CNN "FUNCTION"
	1    3600 7400
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 6132A8B1
P 3800 7050
AR Path="/6132A8B1" Ref="Q?"  Part="1" 
AR Path="/60C56809/6132A8B1" Ref="Q4"  Part="1" 
AR Path="/6114C59C/6132A8B1" Ref="Q10"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8B1" Ref="Q?"  Part="1" 
F 0 "Q?" H 3991 7096 50  0000 L CNN
F 1 "Q_NPN_BEC" H 3991 7005 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4000 7150 50  0001 C CNN
F 3 "~" H 3800 7050 50  0001 C CNN
F 4 "PMBT3904,215" H 3800 7050 50  0001 C CNN "MPN"
	1    3800 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6132A8B2
P 3000 7050
AR Path="/6132A8B2" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8B2" Ref="R19"  Part="1" 
AR Path="/6114C59C/6132A8B2" Ref="R36"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8B2" Ref="R?"  Part="1" 
F 0 "R?" H 2930 7004 50  0000 R CNN
F 1 "4.7k" H 2930 7095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2930 7050 50  0001 C CNN
F 3 "~" H 3000 7050 50  0001 C CNN
	1    3000 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 7400 3900 7550
Wire Wire Line
	2850 7050 2700 7050
Wire Wire Line
	3900 6850 3900 6700
Text Label 2700 7050 2    50   ~ 0
NB_RESET
Wire Wire Line
	3450 7400 3300 7400
Wire Wire Line
	3300 7050 3150 7050
Wire Wire Line
	3300 7050 3600 7050
Connection ~ 3300 7050
Wire Wire Line
	3750 7400 3900 7400
Wire Wire Line
	3900 7250 3900 7400
Wire Wire Line
	3300 7050 3300 7400
Connection ~ 3900 7400
$Comp
L Connector:SIM_Card J?
U 1 1 6132A8B3
P 10100 3950
AR Path="/6132A8B3" Ref="J?"  Part="1" 
AR Path="/60C56809/6132A8B3" Ref="J1"  Part="1" 
AR Path="/6114C59C/6132A8B3" Ref="J2"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8B3" Ref="J?"  Part="1" 
F 0 "J?" H 10730 4050 50  0000 L CNN
F 1 "SIM_Card" H 10730 3959 50  0000 L CNN
F 2 "custom:SIM_CARD_H_DS1138_06_06SS4BSR" H 10100 4300 50  0001 C CNN
F 3 " ~" H 10050 3950 50  0001 C CNN
	1    10100 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6132A8B4
P 8900 2400
AR Path="/6132A8B4" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8B4" Ref="C16"  Part="1" 
AR Path="/6114C59C/6132A8B4" Ref="C32"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8B4" Ref="C?"  Part="1" 
F 0 "C?" H 9015 2446 50  0000 L CNN
F 1 "100nF" H 9015 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8938 2250 50  0001 C CNN
F 3 "~" H 8900 2400 50  0001 C CNN
	1    8900 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6132A8B5
P 9350 2400
AR Path="/6132A8B5" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8B5" Ref="C17"  Part="1" 
AR Path="/6114C59C/6132A8B5" Ref="C34"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8B5" Ref="C?"  Part="1" 
F 0 "C?" H 9465 2446 50  0000 L CNN
F 1 "33pF" H 9465 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9388 2250 50  0001 C CNN
F 3 "~" H 9350 2400 50  0001 C CNN
	1    9350 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2250 8900 2100
Wire Wire Line
	9350 2250 9350 2100
Wire Wire Line
	8900 2550 8900 2700
Wire Wire Line
	9350 2550 9350 2700
$Comp
L Device:C C?
U 1 1 60C6FB91
P 9850 2400
AR Path="/60C6FB91" Ref="C?"  Part="1" 
AR Path="/60C56809/60C6FB91" Ref="C20"  Part="1" 
AR Path="/6114C59C/60C6FB91" Ref="C36"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60C6FB91" Ref="C?"  Part="1" 
F 0 "C?" H 9965 2446 50  0000 L CNN
F 1 "10pF" H 9965 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9888 2250 50  0001 C CNN
F 3 "~" H 9850 2400 50  0001 C CNN
	1    9850 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 2250 9850 2100
Wire Wire Line
	9850 2550 9850 2700
$Comp
L Device:CP C12
U 1 1 60C70BE5
P 8450 2400
AR Path="/60C56809/60C70BE5" Ref="C12"  Part="1" 
AR Path="/6114C59C/60C70BE5" Ref="C30"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60C70BE5" Ref="C?"  Part="1" 
F 0 "C?" H 8568 2446 50  0000 L CNN
F 1 "100uF" H 8568 2355 50  0000 L CNN
F 2 "" H 8488 2250 50  0001 C CNN
F 3 "~" H 8450 2400 50  0001 C CNN
	1    8450 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2250 8450 2100
Wire Wire Line
	8450 2550 8450 2700
Wire Wire Line
	8450 2700 8900 2700
Wire Wire Line
	8900 2700 9350 2700
Connection ~ 8900 2700
Wire Wire Line
	9350 2700 9850 2700
Connection ~ 9350 2700
Wire Wire Line
	8450 2100 8900 2100
Wire Wire Line
	8900 2100 9350 2100
Connection ~ 8900 2100
Wire Wire Line
	9850 2100 9350 2100
Connection ~ 9350 2100
Text HLabel 1050 700  0    50   Input ~ 0
VCC_R
Text HLabel 1050 850  0    50   Input ~ 0
GND_R
Wire Wire Line
	1050 700  1200 700 
Wire Wire Line
	1050 850  1200 850 
Text Label 1200 700  0    50   ~ 0
VCC_R
Text Label 1200 850  0    50   ~ 0
GND_R
Text Label 2750 3550 2    50   ~ 0
GND_R
Text Label 6250 1850 0    50   ~ 0
GND_R
Text Label 6250 1950 0    50   ~ 0
GND_R
Text Label 9050 2100 0    50   ~ 0
VCC_R
Text Label 9050 2700 0    50   ~ 0
GND_R
Text Label 3900 7550 0    50   ~ 0
GND_R
$Comp
L custom:ANT1204F005R0915A ANT?
U 1 1 60C95825
P 9600 900
AR Path="/60C95825" Ref="ANT?"  Part="1" 
AR Path="/60C56809/60C95825" Ref="ANT1"  Part="1" 
AR Path="/6114C59C/60C95825" Ref="ANT2"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60C95825" Ref="ANT?"  Part="1" 
F 0 "ANT?" H 9600 1197 60  0000 C CNN
F 1 "ANT1204F005R0915A" H 9600 1091 60  0000 C CNN
F 2 "custom:antenna_test1" H 9600 1091 60  0001 C CNN
F 3 "" V 9600 900 60  0000 C CNN
	1    9600 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 950  9200 950 
Wire Wire Line
	10000 850  10150 850 
Text Label 10150 850  0    50   ~ 0
GND_R
Text Label 9050 950  0    60   ~ 0
ANT
$Comp
L wise_farm_nb_iot_1-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue C?
U 1 1 60C95830
P 8450 1250
AR Path="/60C95830" Ref="C?"  Part="1" 
AR Path="/60C56809/60C95830" Ref="C15"  Part="1" 
AR Path="/6114C59C/60C95830" Ref="C28"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60C95830" Ref="C?"  Part="1" 
F 0 "C?" H 8550 1250 50  0000 L CNN
F 1 "X pF" H 8475 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8488 1100 30  0001 C CNN
F 3 "Kit smd capacitors" H 8450 1250 60  0001 C CNN
F 4 "---" H 8450 1250 60  0001 C CNN "digikey"
	1    8450 1250
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_1-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue C?
U 1 1 6132A8BA
P 9050 1250
AR Path="/6132A8BA" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8BA" Ref="C18"  Part="1" 
AR Path="/6114C59C/6132A8BA" Ref="C29"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8BA" Ref="C?"  Part="1" 
F 0 "C?" H 8800 1250 50  0000 L CNN
F 1 "X pF" H 8700 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9088 1100 30  0001 C CNN
F 3 "Kit smd capacitors" H 9050 1250 60  0001 C CNN
F 4 "---" H 9050 1250 60  0001 C CNN "digikey"
	1    9050 1250
	-1   0    0    -1  
$EndComp
Text Notes 8950 650  2    60   ~ 0
Dual band PI Macthing Network
$Comp
L wise_farm_nb_iot_1-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue L?
U 1 1 6132A8BB
P 8750 950
AR Path="/6132A8BB" Ref="L?"  Part="1" 
AR Path="/60C56809/6132A8BB" Ref="L3"  Part="1" 
AR Path="/6114C59C/6132A8BB" Ref="L5"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8BB" Ref="L?"  Part="1" 
F 0 "L?" V 8700 950 50  0000 C CNN
F 1 "L" V 8825 950 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 8750 950 50  0001 C CNN
F 3 "Kit smd inductors" H 8750 950 50  0001 C CNN
F 4 "---" V 8750 950 60  0001 C CNN "digikey"
	1    8750 950 
	0    1    -1   0   
$EndComp
Wire Wire Line
	9050 1400 9050 1550
Wire Wire Line
	8450 1400 8450 1550
Wire Wire Line
	8450 1100 8450 950 
Wire Wire Line
	8450 950  8600 950 
Wire Wire Line
	9050 1100 9050 950 
Wire Wire Line
	9050 950  8900 950 
Connection ~ 9050 950 
Connection ~ 8450 950 
Text HLabel 1050 1000 0    50   Input ~ 0
ON_OFF
Text HLabel 1050 1150 0    50   Input ~ 0
RESET_R
Wire Wire Line
	1050 1000 1200 1000
Wire Wire Line
	1050 1150 1200 1150
Wire Wire Line
	6100 3050 6250 3050
Text Label 4050 6700 0    50   ~ 0
RESET
Text Label 6250 3050 0    50   ~ 0
RESET
Text HLabel 1050 1450 0    50   Output ~ 0
TXD_R
Text HLabel 1050 1600 0    50   Input ~ 0
RXD_R
Wire Wire Line
	1050 1450 1200 1450
Wire Wire Line
	1050 1600 1200 1600
Text Label 1200 1150 0    50   ~ 0
NB_RESET
Text Label 1200 1000 0    50   ~ 0
NB_ON_OFF
Text Label 1200 1450 0    50   ~ 0
NB_TXD
Text Label 1200 1600 0    50   ~ 0
NB_RXD
Wire Wire Line
	5100 4300 5100 4450
Wire Wire Line
	2900 3050 2750 3050
Text Label 2750 3050 2    50   ~ 0
VDD_EXT
Wire Wire Line
	5000 4450 5000 4300
$Comp
L Device:C C?
U 1 1 6132A8C7
P 9150 4650
AR Path="/6132A8C7" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8C7" Ref="C22"  Part="1" 
AR Path="/6114C59C/6132A8C7" Ref="C35"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C7" Ref="C?"  Part="1" 
F 0 "C?" H 9265 4696 50  0000 L CNN
F 1 "33pF" H 9265 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9188 4500 50  0001 C CNN
F 3 "~" H 9150 4650 50  0001 C CNN
	1    9150 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6132A8C8
P 8700 4650
AR Path="/6132A8C8" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8C8" Ref="C21"  Part="1" 
AR Path="/6114C59C/6132A8C8" Ref="C33"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C8" Ref="C?"  Part="1" 
F 0 "C?" H 8815 4696 50  0000 L CNN
F 1 "33pF" H 8815 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8738 4500 50  0001 C CNN
F 3 "~" H 8700 4650 50  0001 C CNN
	1    8700 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 60DC078B
P 8300 4650
AR Path="/60DC078B" Ref="C?"  Part="1" 
AR Path="/60C56809/60DC078B" Ref="C19"  Part="1" 
AR Path="/6114C59C/60DC078B" Ref="C31"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60DC078B" Ref="C?"  Part="1" 
F 0 "C?" H 8415 4696 50  0000 L CNN
F 1 "33pF" H 8415 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8338 4500 50  0001 C CNN
F 3 "~" H 8300 4650 50  0001 C CNN
	1    8300 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 4800 8300 4950
Wire Wire Line
	8300 4950 8700 4950
Wire Wire Line
	9150 4950 9150 4800
Wire Wire Line
	8700 4800 8700 4950
Connection ~ 8700 4950
Wire Wire Line
	8700 4950 9150 4950
Wire Wire Line
	9150 4500 9150 4150
Wire Wire Line
	8700 3850 8700 4500
Wire Wire Line
	8300 3750 8300 4500
$Comp
L Device:R R?
U 1 1 6132A8CA
P 8000 3750
AR Path="/6132A8CA" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8CA" Ref="R26"  Part="1" 
AR Path="/6114C59C/6132A8CA" Ref="R43"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8CA" Ref="R?"  Part="1" 
F 0 "R?" H 7930 3704 50  0000 R CNN
F 1 "22R" H 7930 3795 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7930 3750 50  0001 C CNN
F 3 "~" H 8000 3750 50  0001 C CNN
	1    8000 3750
	0    -1   1    0   
$EndComp
Wire Wire Line
	7700 3850 8650 3850
$Comp
L Device:R R?
U 1 1 6132A8CB
P 7550 3850
AR Path="/6132A8CB" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8CB" Ref="R25"  Part="1" 
AR Path="/6114C59C/6132A8CB" Ref="R42"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8CB" Ref="R?"  Part="1" 
F 0 "R?" H 7480 3804 50  0000 R CNN
F 1 "22R" H 7480 3895 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7480 3850 50  0001 C CNN
F 3 "~" H 7550 3850 50  0001 C CNN
	1    7550 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6132A8CC
P 8000 4150
AR Path="/6132A8CC" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8CC" Ref="R27"  Part="1" 
AR Path="/6114C59C/6132A8CC" Ref="R44"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8CC" Ref="R?"  Part="1" 
F 0 "R?" H 7930 4104 50  0000 R CNN
F 1 "22R" H 7930 4195 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7930 4150 50  0001 C CNN
F 3 "~" H 8000 4150 50  0001 C CNN
	1    8000 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 3750 7250 3750
Wire Wire Line
	7400 3850 7250 3850
Wire Wire Line
	7850 4150 7250 4150
$Comp
L Device:R R?
U 1 1 60E1B169
P 8950 3650
AR Path="/60E1B169" Ref="R?"  Part="1" 
AR Path="/60C56809/60E1B169" Ref="R28"  Part="1" 
AR Path="/6114C59C/60E1B169" Ref="R45"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60E1B169" Ref="R?"  Part="1" 
F 0 "R?" H 8880 3604 50  0000 R CNN
F 1 "10k" H 8880 3695 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8880 3650 50  0001 C CNN
F 3 "~" H 8950 3650 50  0001 C CNN
	1    8950 3650
	0    -1   1    0   
$EndComp
Connection ~ 8650 4150
Wire Wire Line
	8650 4150 8150 4150
Wire Wire Line
	8800 3650 8650 3650
Wire Wire Line
	8650 3650 8650 3850
$Comp
L Device:C C?
U 1 1 6132A8CE
P 9550 4650
AR Path="/6132A8CE" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8CE" Ref="C23"  Part="1" 
AR Path="/6114C59C/6132A8CE" Ref="C37"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8CE" Ref="C?"  Part="1" 
F 0 "C?" H 9665 4696 50  0000 L CNN
F 1 "100nF" H 9665 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9588 4500 50  0001 C CNN
F 3 "~" H 9550 4650 50  0001 C CNN
	1    9550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 4800 9550 4950
Text Label 9550 4950 0    50   ~ 0
GND_R
NoConn ~ 9600 4050
Text Label 8600 4950 2    50   ~ 0
GND_R
Text Label 7250 3750 2    50   ~ 0
SIM1_RST
Text Label 7250 3850 2    50   ~ 0
SIM1_CLK
Text Label 7250 4150 2    50   ~ 0
SIM1_DATA
Text Label 2750 2750 2    50   ~ 0
SIM1_RST
Wire Wire Line
	2900 2750 2750 2750
Wire Wire Line
	2900 2850 2750 2850
Wire Wire Line
	2900 2650 2750 2650
Wire Wire Line
	2900 2950 2750 2950
Text Label 2750 2950 2    50   ~ 0
SIM1_VCC
Text Label 2750 2650 2    50   ~ 0
SIM1_CLK
Text Label 2750 2850 2    50   ~ 0
SIM1_DATA
Text Notes 8550 3300 0    50   ~ 0
(U)SIM1 Interface
Text Notes 8000 1900 0    50   ~ 0
The input voltage of VBAT ranges from 3.4V to 4.2V.\nThe module drains a maximum current of approx. 1.6A during GSM burst.\nThe minimum width of VBAT trace is recommended to be 2mm.
$Comp
L Device:C C?
U 1 1 6132A8D2
P 2600 5400
AR Path="/6132A8D2" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8D2" Ref="C10"  Part="1" 
AR Path="/6114C59C/6132A8D2" Ref="C26"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8D2" Ref="C?"  Part="1" 
F 0 "C?" H 2715 5446 50  0000 L CNN
F 1 "100nF" H 2715 5355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2638 5250 50  0001 C CNN
F 3 "~" H 2600 5400 50  0001 C CNN
	1    2600 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 5550 2600 5700
Wire Wire Line
	2600 5250 2600 5100
Text Label 2600 5700 0    50   ~ 0
GND_R
Text Label 2600 5100 0    50   ~ 0
VDD_EXT
Text Notes 1950 6050 0    50   ~ 0
It is recommended to use VDD_EXT for \nthe external pull-up power supply for I/O\nnd add a bypass capacitor of 2.2uF in parallel
Text Notes 7900 3050 0    50   ~ 0
These capacitor are arranged with capacitances in ascending order. \nCapacitor with lowest capacitance is closest to VBAT pin and all \ncapacitors should be as close to VBAT pin as possible.\n
Text Notes 2600 6800 0    50   ~ 0
Reset Reference Circuit
$Comp
L Device:R R?
U 1 1 6132A8D3
P 1850 2650
AR Path="/6132A8D3" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8D3" Ref="R18"  Part="1" 
AR Path="/6114C59C/6132A8D3" Ref="R35"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8D3" Ref="R?"  Part="1" 
F 0 "R?" H 1920 2696 50  0000 L CNN
F 1 "2.2k" H 1920 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1780 2650 50  0001 C CNN
F 3 "~" H 1850 2650 50  0001 C CNN
	1    1850 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2800 1850 3000
Wire Wire Line
	1850 3300 1850 3450
$Comp
L Device:R R?
U 1 1 6132A8D4
P 1550 4000
AR Path="/6132A8D4" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8D4" Ref="R17"  Part="1" 
AR Path="/6114C59C/6132A8D4" Ref="R34"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8D4" Ref="R?"  Part="1" 
F 0 "R?" H 1480 3954 50  0000 R CNN
F 1 "47k" H 1480 4045 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1480 4000 50  0001 C CNN
F 3 "~" H 1550 4000 50  0001 C CNN
F 4 "RS-03K1001FT" H 1550 4000 50  0001 C CNN "MPN"
F 5 "ST" H 1550 4000 50  0001 C CNN "FUNCTION"
	1    1550 4000
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 6132A8D5
P 1750 3650
AR Path="/6132A8D5" Ref="Q?"  Part="1" 
AR Path="/60C56809/6132A8D5" Ref="Q3"  Part="1" 
AR Path="/6114C59C/6132A8D5" Ref="Q9"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8D5" Ref="Q?"  Part="1" 
F 0 "Q?" H 1941 3696 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1941 3605 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1950 3750 50  0001 C CNN
F 3 "~" H 1750 3650 50  0001 C CNN
F 4 "PMBT3904,215" H 1750 3650 50  0001 C CNN "MPN"
	1    1750 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6132A8D6
P 950 3650
AR Path="/6132A8D6" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8D6" Ref="R14"  Part="1" 
AR Path="/6114C59C/6132A8D6" Ref="R30"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8D6" Ref="R?"  Part="1" 
F 0 "R?" H 880 3604 50  0000 R CNN
F 1 "4.7k" H 880 3695 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 880 3650 50  0001 C CNN
F 3 "~" H 950 3650 50  0001 C CNN
	1    950  3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 4000 1850 4150
Wire Wire Line
	800  3650 650  3650
Text Label 650  3650 3    50   ~ 0
S_LED
Wire Wire Line
	1400 4000 1250 4000
Wire Wire Line
	1250 3650 1100 3650
Wire Wire Line
	1250 3650 1550 3650
Connection ~ 1250 3650
Wire Wire Line
	1700 4000 1850 4000
Wire Wire Line
	1850 3850 1850 4000
Wire Wire Line
	1250 3650 1250 4000
Connection ~ 1850 4000
Text Label 1850 4150 0    50   ~ 0
GND_R
Text Notes 700  3400 0    50   ~ 0
Network Status Indication
Text Notes 6800 5450 0    50   ~ 0
The transistor circuit solution is not suitable for applications with high baud rates exceeding 460Kbps\nIn order to ensure the effective operation of the transistor circuit, BJT_CTL needs to select the lower of the two levels\nWhen MCU_VDD<VDD_EXT, BJT_CTL=MCU_VDD. (If low-power design is required, a MCU_IO\nshould be selected as BJT_CTL. When the module enters deep sleep, MCU controls MCU_IO as low level.\nIf high baud rate is needed, it is highly recommended to install two 1nF capacitors on transistor circiuts.
Text Label 9150 3650 0    50   ~ 0
SIM1_VCC
$Comp
L Connector:TestPoint TP?
U 1 1 60F65030
P 7400 2200
AR Path="/60F65030" Ref="TP?"  Part="1" 
AR Path="/60C56809/60F65030" Ref="TP1"  Part="1" 
AR Path="/6114C59C/60F65030" Ref="TP17"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60F65030" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2388 50  0000 L CNN
F 1 "TestPoint" H 7458 2227 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2200 50  0001 C CNN
F 3 "~" H 7600 2200 50  0001 C CNN
	1    7400 2200
	0    1    -1   0   
$EndComp
Wire Wire Line
	7400 2200 7250 2200
Wire Wire Line
	7400 2300 7250 2300
Wire Wire Line
	7400 2400 7250 2400
$Comp
L Connector:TestPoint TP?
U 1 1 60F6503C
P 7400 2400
AR Path="/60F6503C" Ref="TP?"  Part="1" 
AR Path="/60C56809/60F6503C" Ref="TP9"  Part="1" 
AR Path="/6114C59C/60F6503C" Ref="TP19"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60F6503C" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2588 50  0000 L CNN
F 1 "TestPoint" H 7458 2427 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2400 50  0001 C CNN
F 3 "~" H 7600 2400 50  0001 C CNN
	1    7400 2400
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60F65042
P 7400 2300
AR Path="/60F65042" Ref="TP?"  Part="1" 
AR Path="/60C56809/60F65042" Ref="TP2"  Part="1" 
AR Path="/6114C59C/60F65042" Ref="TP18"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60F65042" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2488 50  0000 L CNN
F 1 "TestPoint" H 7458 2327 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2300 50  0001 C CNN
F 3 "~" H 7600 2300 50  0001 C CNN
	1    7400 2300
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60F65048
P 7400 2500
AR Path="/60F65048" Ref="TP?"  Part="1" 
AR Path="/60C56809/60F65048" Ref="TP10"  Part="1" 
AR Path="/6114C59C/60F65048" Ref="TP20"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60F65048" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2688 50  0000 L CNN
F 1 "TestPoint" H 7458 2527 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2500 50  0001 C CNN
F 3 "~" H 7600 2500 50  0001 C CNN
	1    7400 2500
	0    1    -1   0   
$EndComp
Wire Wire Line
	7400 2500 7250 2500
Wire Wire Line
	7400 2600 7250 2600
Wire Wire Line
	7400 2700 7250 2700
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8DB
P 7400 2700
AR Path="/6132A8DB" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8DB" Ref="TP12"  Part="1" 
AR Path="/6114C59C/6132A8DB" Ref="TP22"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8DB" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2888 50  0000 L CNN
F 1 "TestPoint" H 7458 2727 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2700 50  0001 C CNN
F 3 "~" H 7600 2700 50  0001 C CNN
	1    7400 2700
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8DC
P 7400 2600
AR Path="/6132A8DC" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8DC" Ref="TP11"  Part="1" 
AR Path="/6114C59C/6132A8DC" Ref="TP21"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8DC" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2788 50  0000 L CNN
F 1 "TestPoint" H 7458 2627 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2600 50  0001 C CNN
F 3 "~" H 7600 2600 50  0001 C CNN
	1    7400 2600
	0    1    -1   0   
$EndComp
Text Label 7250 2200 2    50   ~ 0
ON_OFF
Text Label 7250 2300 2    50   ~ 0
NB_RESET
Text Label 7250 2400 2    50   ~ 0
NB_TXD
Text Label 7250 2500 2    50   ~ 0
NB_RXD
Text Notes 1550 1750 0    50   ~ 0
(MCU_TXD / MCU_RXD)
Text Label 7250 2600 2    50   ~ 0
BJT_CTRL
Text Label 7250 2700 2    50   ~ 0
WAKE
$Comp
L wise_farm_nb_iot_1-rescue:C-ln_hope_rf_p01-rescue-ble_nrf52838-rescue C?
U 1 1 6132A8DD
P 8050 1250
AR Path="/6132A8DD" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8DD" Ref="C11"  Part="1" 
AR Path="/6114C59C/6132A8DD" Ref="C27"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8DD" Ref="C?"  Part="1" 
F 0 "C?" H 8150 1250 50  0000 L CNN
F 1 "X pF" H 8075 1150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8088 1100 30  0001 C CNN
F 3 "Kit smd capacitors" H 8050 1250 60  0001 C CNN
F 4 "---" H 8050 1250 60  0001 C CNN "digikey"
	1    8050 1250
	-1   0    0    -1  
$EndComp
$Comp
L wise_farm_nb_iot_1-rescue:L-ln_hope_rf_p01-rescue-ble_nrf52838-rescue L?
U 1 1 60F994C0
P 7650 950
AR Path="/60F994C0" Ref="L?"  Part="1" 
AR Path="/60C56809/60F994C0" Ref="L2"  Part="1" 
AR Path="/6114C59C/60F994C0" Ref="L4"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60F994C0" Ref="L?"  Part="1" 
F 0 "L?" V 7600 950 50  0000 C CNN
F 1 "L" V 7725 950 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 7650 950 50  0001 C CNN
F 3 "Kit smd inductors" H 7650 950 50  0001 C CNN
F 4 "---" V 7650 950 60  0001 C CNN "digikey"
	1    7650 950 
	0    1    -1   0   
$EndComp
Wire Wire Line
	7800 950  8050 950 
Wire Wire Line
	8050 950  8050 1100
Wire Wire Line
	8050 1400 8050 1550
Text Label 8050 1550 0    50   ~ 0
GND_R
Text Label 8450 1550 0    50   ~ 0
GND_R
Text Label 9050 1550 0    50   ~ 0
GND_R
Wire Wire Line
	8050 950  8450 950 
Connection ~ 8050 950 
Wire Wire Line
	7400 2800 7250 2800
$Comp
L Connector:TestPoint TP?
U 1 1 60FB7B5F
P 7400 2800
AR Path="/60FB7B5F" Ref="TP?"  Part="1" 
AR Path="/60C56809/60FB7B5F" Ref="TP13"  Part="1" 
AR Path="/6114C59C/60FB7B5F" Ref="TP23"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60FB7B5F" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 2988 50  0000 L CNN
F 1 "TestPoint" H 7458 2827 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2800 50  0001 C CNN
F 3 "~" H 7600 2800 50  0001 C CNN
	1    7400 2800
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 60FB7B65
P 7400 2900
AR Path="/60FB7B65" Ref="TP?"  Part="1" 
AR Path="/60C56809/60FB7B65" Ref="TP14"  Part="1" 
AR Path="/6114C59C/60FB7B65" Ref="TP24"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60FB7B65" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 3088 50  0000 L CNN
F 1 "TestPoint" H 7458 2927 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2900 50  0001 C CNN
F 3 "~" H 7600 2900 50  0001 C CNN
	1    7400 2900
	0    1    -1   0   
$EndComp
Wire Wire Line
	7400 2900 7250 2900
Wire Wire Line
	7400 3000 7250 3000
Wire Wire Line
	7400 3100 7250 3100
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8E1
P 7400 3100
AR Path="/6132A8E1" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8E1" Ref="TP16"  Part="1" 
AR Path="/6114C59C/6132A8E1" Ref="TP26"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8E1" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 3288 50  0000 L CNN
F 1 "TestPoint" H 7458 3127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 3100 50  0001 C CNN
F 3 "~" H 7600 3100 50  0001 C CNN
	1    7400 3100
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6132A8E2
P 7400 3000
AR Path="/6132A8E2" Ref="TP?"  Part="1" 
AR Path="/60C56809/6132A8E2" Ref="TP15"  Part="1" 
AR Path="/6114C59C/6132A8E2" Ref="TP25"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8E2" Ref="TP?"  Part="1" 
F 0 "TP?" V 7400 3188 50  0000 L CNN
F 1 "TestPoint" H 7458 3027 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 3000 50  0001 C CNN
F 3 "~" H 7600 3000 50  0001 C CNN
	1    7400 3000
	0    1    -1   0   
$EndComp
Text Label 7250 2800 2    50   ~ 0
SIM1_RST
Text Label 7250 2900 2    50   ~ 0
SIM1_CLK
Text Label 7250 3000 2    50   ~ 0
SIM1_DATA
Text Label 7250 3100 2    50   ~ 0
SIM1_VDD
Wire Wire Line
	9850 2100 10450 2100
Wire Wire Line
	10450 2100 10450 2200
Connection ~ 9850 2100
Wire Wire Line
	10450 2700 9850 2700
Connection ~ 9850 2700
$Comp
L custom:MSQA6V1W5T2G D?
U 1 1 6106FE17
P 8900 5950
AR Path="/60C56809/6106FE17" Ref="D?"  Part="1" 
AR Path="/6114C59C/6106FE17" Ref="D6"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6106FE17" Ref="D?"  Part="1" 
F 0 "D?" H 8850 6337 60  0000 C CNN
F 1 "MSQA6V1W5T2G" H 8850 6231 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5" H 9100 6150 60  0001 L CNN
F 3 "" H 9100 6250 60  0001 L CNN
	1    8900 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 5850 8250 5850
Wire Wire Line
	8400 5950 8250 5950
Wire Wire Line
	8250 6050 8400 6050
Wire Wire Line
	9300 5850 9450 5850
Wire Wire Line
	9300 6050 9450 6050
Text Label 9600 3750 2    50   ~ 0
SIM1_RST_TVS
Text Label 8650 3850 2    50   ~ 0
SIM1_CLK_TVS
Text Label 8750 4150 2    50   ~ 0
SIM1_DATA_TVS
Text Label 9450 5850 0    50   ~ 0
SIM1_VDD
Text Label 8250 5950 2    50   ~ 0
GND_R
Text Label 8250 5850 2    50   ~ 0
SIM1_DATA_TVS
Connection ~ 8650 3850
Wire Wire Line
	8650 3850 8700 3850
Wire Wire Line
	8650 3850 8650 4150
Text Label 8250 6050 2    50   ~ 0
SIM1_CLK_TVS
Text Label 9450 6050 0    50   ~ 0
SIM1_RST_TVS
$Comp
L custom:NE310H2 U5
U 1 1 611F78F2
P 4500 2650
AR Path="/6114C59C/611F78F2" Ref="U5"  Part="1" 
AR Path="/6083CF6C/6083E0B5/611F78F2" Ref="U?"  Part="1" 
F 0 "U?" H 4500 2850 50  0000 C CNN
F 1 "NE310H2" H 4500 2450 50  0000 C CNN
F 2 "" H 4500 2650 50  0001 C CNN
F 3 "" H 4500 2650 50  0001 C CNN
	1    4500 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2150 6100 2150
Wire Wire Line
	6250 2250 6100 2250
Text Label 6250 2150 0    50   ~ 0
GND_R
Text Label 6250 2250 0    50   ~ 0
GND_R
Wire Wire Line
	6250 2750 6100 2750
Wire Wire Line
	6250 2850 6100 2850
Text Label 6250 2750 0    50   ~ 0
GND_R
Text Label 6250 2850 0    50   ~ 0
GND_R
Wire Wire Line
	6250 2450 6100 2450
Wire Wire Line
	6250 3450 6100 3450
Text Label 6250 2450 0    50   ~ 0
GND_R
Text Label 6250 3450 0    50   ~ 0
GND_R
Text Label 5200 800  1    50   ~ 0
GND_R
Text Label 5100 800  1    50   ~ 0
GND_R
Wire Wire Line
	5200 800  5200 950 
Wire Wire Line
	5100 800  5100 950 
Text Label 4900 800  1    50   ~ 0
GND_R
Text Label 4800 800  1    50   ~ 0
GND_R
Wire Wire Line
	4900 800  4900 950 
Wire Wire Line
	4800 800  4800 950 
Text Label 4500 800  1    50   ~ 0
GND_R
Text Label 4400 800  1    50   ~ 0
GND_R
Wire Wire Line
	4500 800  4500 950 
Wire Wire Line
	4400 800  4400 950 
Text Label 3900 800  1    50   ~ 0
GND_R
Text Label 3800 800  1    50   ~ 0
GND_R
Wire Wire Line
	3900 800  3900 950 
Wire Wire Line
	3800 800  3800 950 
Text Label 4200 800  1    50   ~ 0
GND_R
Text Label 4100 800  1    50   ~ 0
GND_R
Wire Wire Line
	4200 800  4200 950 
Wire Wire Line
	4100 800  4100 950 
Text Label 4700 800  1    50   ~ 0
GND_R
Wire Wire Line
	4700 800  4700 950 
Wire Wire Line
	2750 3250 2900 3250
Text Label 2750 3250 2    50   ~ 0
GND_R
Wire Wire Line
	2750 3350 2900 3350
Text Label 2750 3350 2    50   ~ 0
GND_R
Text Label 3850 2850 2    50   ~ 0
GND_R
Text Label 3850 2450 2    50   ~ 0
GND_R
Text Label 4350 1900 1    50   ~ 0
GND_R
Text Label 4650 1900 1    50   ~ 0
GND_R
Text Label 5150 2450 0    50   ~ 0
GND_R
Text Label 5150 2850 0    50   ~ 0
GND_R
Text Label 4700 3400 3    50   ~ 0
GND_R
Text Label 4400 3400 3    50   ~ 0
GND_R
Text Label 4600 3400 3    50   ~ 0
IO1
Text Label 4800 3400 3    50   ~ 0
IO2
Text Label 5150 2650 0    50   ~ 0
WAKE
Text Label 5150 2750 0    50   ~ 0
ON_OFF
Text Label 4450 1900 1    50   ~ 0
IO4
Text Label 4250 1900 1    50   ~ 0
IO3
Text Label 4550 1900 1    50   ~ 0
IO5
Text Label 4750 1900 1    50   ~ 0
IO6
Text Label 3850 2650 2    50   ~ 0
DVI_MCLK
$Comp
L Device:LED D?
U 1 1 613F7D12
P 1850 3150
AR Path="/613F7D12" Ref="D?"  Part="1" 
AR Path="/60C56809/613F7D12" Ref="D?"  Part="1" 
AR Path="/6114C59C/613F7D12" Ref="D8"  Part="1" 
AR Path="/6083CF6C/6083E0B5/613F7D12" Ref="D?"  Part="1" 
F 0 "D?" V 1889 3033 50  0000 R CNN
F 1 "LED" V 1798 3033 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1850 3150 50  0001 C CNN
F 3 "~" H 1850 3150 50  0001 C CNN
F 4 "LTST-C190KRKT-TH" H 1850 3150 50  0001 C CNN "MPN"
	1    1850 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 2500 1850 2350
Text Label 1850 2350 1    50   ~ 0
VCC_R
Text Notes 700  4400 0    50   ~ 0
Level Translation - Transistor Solution
Text Notes 1850 7050 0    50   ~ 0
(MCU_RXD)
Text Notes 1800 5650 0    50   ~ 0
(MCU_TXD)
Text Label 850  5500 2    50   ~ 0
RXD
Text Label 1850 5500 0    50   ~ 0
NB_RXD
Text Label 1000 4950 2    50   ~ 0
VDD_EXT
Wire Wire Line
	1700 5500 1850 5500
Wire Wire Line
	1000 5500 850  5500
Text Label 1550 4600 0    50   ~ 0
BJT_CTRL
Wire Wire Line
	1500 5050 1500 5200
Wire Wire Line
	1950 5200 1500 5200
Wire Wire Line
	1950 5050 1950 5200
Wire Wire Line
	1950 4600 1950 4750
Wire Wire Line
	1500 4600 1950 4600
Wire Wire Line
	1500 4750 1500 4600
$Comp
L Device:C C?
U 1 1 6132A8C6
P 1950 4900
AR Path="/6132A8C6" Ref="C?"  Part="1" 
AR Path="/60C56809/6132A8C6" Ref="C8"  Part="1" 
AR Path="/6114C59C/6132A8C6" Ref="C25"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C6" Ref="C?"  Part="1" 
F 0 "C?" H 2065 4946 50  0000 L CNN
F 1 "1nF" H 2065 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1988 4750 50  0001 C CNN
F 3 "~" H 1950 4900 50  0001 C CNN
	1    1950 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 5100 1000 4950
Wire Wire Line
	1000 5500 1000 5400
Connection ~ 1000 5500
Wire Wire Line
	1300 5500 1000 5500
$Comp
L Device:R R?
U 1 1 6132A8C5
P 1000 5250
AR Path="/6132A8C5" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8C5" Ref="R7"  Part="1" 
AR Path="/6114C59C/6132A8C5" Ref="R29"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C5" Ref="R?"  Part="1" 
F 0 "R?" H 930 5204 50  0000 R CNN
F 1 "4.7k" H 930 5295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 930 5250 50  0001 C CNN
F 3 "~" H 1000 5250 50  0001 C CNN
	1    1000 5250
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6132A8C4
P 1500 4900
AR Path="/6132A8C4" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8C4" Ref="R15"  Part="1" 
AR Path="/6114C59C/6132A8C4" Ref="R32"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C4" Ref="R?"  Part="1" 
F 0 "R?" H 1430 4854 50  0000 R CNN
F 1 "4.7k" H 1430 4945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1430 4900 50  0001 C CNN
F 3 "~" H 1500 4900 50  0001 C CNN
	1    1500 4900
	-1   0    0    -1  
$EndComp
Text Label 900  6900 2    50   ~ 0
TXD
Wire Wire Line
	1050 6900 900  6900
Wire Wire Line
	800  6600 1250 6600
Wire Wire Line
	800  6450 800  6600
Wire Wire Line
	800  6000 1250 6000
Wire Wire Line
	800  6150 800  6000
$Comp
L Device:C C?
U 1 1 60D26027
P 800 6300
AR Path="/60D26027" Ref="C?"  Part="1" 
AR Path="/60C56809/60D26027" Ref="C4"  Part="1" 
AR Path="/6114C59C/60D26027" Ref="C24"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60D26027" Ref="C?"  Part="1" 
F 0 "C?" H 915 6346 50  0000 L CNN
F 1 "1nF" H 915 6255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 838 6150 50  0001 C CNN
F 3 "~" H 800 6300 50  0001 C CNN
	1    800  6300
	1    0    0    -1  
$EndComp
Text Label 850  6000 0    50   ~ 0
BJT_CTRL
Text Label 1900 6900 0    50   ~ 0
NB_TXD
Wire Wire Line
	1250 6600 1250 6450
Wire Wire Line
	1250 6150 1250 6000
$Comp
L Device:R R?
U 1 1 6132A8C2
P 1250 6300
AR Path="/6132A8C2" Ref="R?"  Part="1" 
AR Path="/60C56809/6132A8C2" Ref="R13"  Part="1" 
AR Path="/6114C59C/6132A8C2" Ref="R31"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6132A8C2" Ref="R?"  Part="1" 
F 0 "R?" H 1180 6254 50  0000 R CNN
F 1 "4.7k" H 1180 6345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1180 6300 50  0001 C CNN
F 3 "~" H 1250 6300 50  0001 C CNN
	1    1250 6300
	-1   0    0    -1  
$EndComp
Connection ~ 1250 6600
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60CFCFBE
P 1250 6800
AR Path="/60CFCFBE" Ref="Q?"  Part="1" 
AR Path="/60C56809/60CFCFBE" Ref="Q1"  Part="1" 
AR Path="/6114C59C/60CFCFBE" Ref="Q7"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60CFCFBE" Ref="Q?"  Part="1" 
F 0 "Q?" H 1441 6846 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1441 6755 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1450 6900 50  0001 C CNN
F 3 "~" H 1250 6800 50  0001 C CNN
F 4 "PMBT3904,215" H 1250 6800 50  0001 C CNN "MPN"
	1    1250 6800
	0    1    1    0   
$EndComp
Connection ~ 1500 5200
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 60CF5774
P 1500 5400
AR Path="/60CF5774" Ref="Q?"  Part="1" 
AR Path="/60C56809/60CF5774" Ref="Q2"  Part="1" 
AR Path="/6114C59C/60CF5774" Ref="Q8"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60CF5774" Ref="Q?"  Part="1" 
F 0 "Q?" H 1691 5446 50  0000 L CNN
F 1 "Q_NPN_BEC" H 1691 5355 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1700 5500 50  0001 C CNN
F 3 "~" H 1500 5400 50  0001 C CNN
F 4 "PMBT3904,215" H 1500 5400 50  0001 C CNN "MPN"
	1    1500 5400
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 6145EA80
P 4250 7050
AR Path="/6145EA80" Ref="C?"  Part="1" 
AR Path="/60C56809/6145EA80" Ref="C?"  Part="1" 
AR Path="/6114C59C/6145EA80" Ref="C38"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6145EA80" Ref="C?"  Part="1" 
F 0 "C?" H 4365 7096 50  0000 L CNN
F 1 "33pF" H 4365 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4288 6900 50  0001 C CNN
F 3 "~" H 4250 7050 50  0001 C CNN
	1    4250 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 6700 4250 6700
Wire Wire Line
	4250 6700 4250 6900
Wire Wire Line
	4250 7200 4250 7400
Wire Wire Line
	4250 7400 3900 7400
$Comp
L Device:R R?
U 1 1 6147554E
P 3900 6400
AR Path="/6147554E" Ref="R?"  Part="1" 
AR Path="/60C56809/6147554E" Ref="R?"  Part="1" 
AR Path="/6114C59C/6147554E" Ref="R48"  Part="1" 
AR Path="/6083CF6C/6083E0B5/6147554E" Ref="R?"  Part="1" 
F 0 "R?" H 3830 6354 50  0000 R CNN
F 1 "4.7k" H 3830 6445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3830 6400 50  0001 C CNN
F 3 "~" H 3900 6400 50  0001 C CNN
	1    3900 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 6250 3900 6100
Wire Wire Line
	3900 6550 3900 6700
Connection ~ 3900 6700
Text Label 3900 6100 0    50   ~ 0
VDD_EXT
$Comp
L Device:R R?
U 1 1 614C0F3A
P 5850 7400
AR Path="/614C0F3A" Ref="R?"  Part="1" 
AR Path="/60C56809/614C0F3A" Ref="R?"  Part="1" 
AR Path="/6114C59C/614C0F3A" Ref="R51"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614C0F3A" Ref="R?"  Part="1" 
F 0 "R?" H 5780 7354 50  0000 R CNN
F 1 "47k" H 5780 7445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5780 7400 50  0001 C CNN
F 3 "~" H 5850 7400 50  0001 C CNN
F 4 "RS-03K1001FT" H 5850 7400 50  0001 C CNN "MPN"
F 5 "ST" H 5850 7400 50  0001 C CNN "FUNCTION"
	1    5850 7400
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 614C0F41
P 6050 7050
AR Path="/614C0F41" Ref="Q?"  Part="1" 
AR Path="/60C56809/614C0F41" Ref="Q?"  Part="1" 
AR Path="/6114C59C/614C0F41" Ref="Q14"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614C0F41" Ref="Q?"  Part="1" 
F 0 "Q?" H 6241 7096 50  0000 L CNN
F 1 "Q_NPN_BEC" H 6241 7005 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6250 7150 50  0001 C CNN
F 3 "~" H 6050 7050 50  0001 C CNN
F 4 "PMBT3904,215" H 6050 7050 50  0001 C CNN "MPN"
	1    6050 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 614C0F47
P 5250 7050
AR Path="/614C0F47" Ref="R?"  Part="1" 
AR Path="/60C56809/614C0F47" Ref="R?"  Part="1" 
AR Path="/6114C59C/614C0F47" Ref="R50"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614C0F47" Ref="R?"  Part="1" 
F 0 "R?" H 5180 7004 50  0000 R CNN
F 1 "4.7k" H 5180 7095 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5180 7050 50  0001 C CNN
F 3 "~" H 5250 7050 50  0001 C CNN
	1    5250 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 7400 6150 7550
Wire Wire Line
	5100 7050 4950 7050
Wire Wire Line
	6150 6850 6150 6700
Text Label 4950 7050 2    50   ~ 0
NB_WAKE
Wire Wire Line
	5700 7400 5550 7400
Wire Wire Line
	5550 7050 5400 7050
Wire Wire Line
	5550 7050 5850 7050
Connection ~ 5550 7050
Wire Wire Line
	6000 7400 6150 7400
Wire Wire Line
	6150 7250 6150 7400
Wire Wire Line
	5550 7050 5550 7400
Connection ~ 6150 7400
Text Label 6150 7550 0    50   ~ 0
GND_R
Text Label 6300 6700 0    50   ~ 0
WAKE
Text Notes 4850 6800 0    50   ~ 0
WAKE Reference Circuit
$Comp
L Device:C C?
U 1 1 614C0F5C
P 6500 7050
AR Path="/614C0F5C" Ref="C?"  Part="1" 
AR Path="/60C56809/614C0F5C" Ref="C?"  Part="1" 
AR Path="/6114C59C/614C0F5C" Ref="C40"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614C0F5C" Ref="C?"  Part="1" 
F 0 "C?" H 6615 7096 50  0000 L CNN
F 1 "33pF" H 6615 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6538 6900 50  0001 C CNN
F 3 "~" H 6500 7050 50  0001 C CNN
	1    6500 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6700 6500 6700
Wire Wire Line
	6500 6700 6500 6900
Wire Wire Line
	6500 7200 6500 7400
Wire Wire Line
	6500 7400 6150 7400
$Comp
L Device:R R?
U 1 1 614C0F66
P 6150 6400
AR Path="/614C0F66" Ref="R?"  Part="1" 
AR Path="/60C56809/614C0F66" Ref="R?"  Part="1" 
AR Path="/6114C59C/614C0F66" Ref="R52"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614C0F66" Ref="R?"  Part="1" 
F 0 "R?" H 6080 6354 50  0000 R CNN
F 1 "4.7k" H 6080 6445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 6400 50  0001 C CNN
F 3 "~" H 6150 6400 50  0001 C CNN
	1    6150 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 6250 6150 6100
Wire Wire Line
	6150 6550 6150 6700
Connection ~ 6150 6700
Text Label 6150 6100 0    50   ~ 0
VDD_EXT
$Comp
L Device:R R?
U 1 1 614D0F7E
P 5050 5900
AR Path="/614D0F7E" Ref="R?"  Part="1" 
AR Path="/60C56809/614D0F7E" Ref="R?"  Part="1" 
AR Path="/6114C59C/614D0F7E" Ref="R47"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614D0F7E" Ref="R?"  Part="1" 
F 0 "R?" H 4980 5854 50  0000 R CNN
F 1 "47k" H 4980 5945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4980 5900 50  0001 C CNN
F 3 "~" H 5050 5900 50  0001 C CNN
F 4 "RS-03K1001FT" H 5050 5900 50  0001 C CNN "MPN"
F 5 "ST" H 5050 5900 50  0001 C CNN "FUNCTION"
	1    5050 5900
	0    1    -1   0   
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 614D0F85
P 5250 5550
AR Path="/614D0F85" Ref="Q?"  Part="1" 
AR Path="/60C56809/614D0F85" Ref="Q?"  Part="1" 
AR Path="/6114C59C/614D0F85" Ref="Q13"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614D0F85" Ref="Q?"  Part="1" 
F 0 "Q?" H 5441 5596 50  0000 L CNN
F 1 "Q_NPN_BEC" H 5441 5505 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 5650 50  0001 C CNN
F 3 "~" H 5250 5550 50  0001 C CNN
F 4 "PMBT3904,215" H 5250 5550 50  0001 C CNN "MPN"
	1    5250 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 614D0F8B
P 4450 5550
AR Path="/614D0F8B" Ref="R?"  Part="1" 
AR Path="/60C56809/614D0F8B" Ref="R?"  Part="1" 
AR Path="/6114C59C/614D0F8B" Ref="R46"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614D0F8B" Ref="R?"  Part="1" 
F 0 "R?" H 4380 5504 50  0000 R CNN
F 1 "4.7k" H 4380 5595 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4380 5550 50  0001 C CNN
F 3 "~" H 4450 5550 50  0001 C CNN
	1    4450 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 5900 5350 6050
Wire Wire Line
	4300 5550 4150 5550
Wire Wire Line
	5350 5350 5350 5200
Text Label 4150 5550 2    50   ~ 0
NB_ON_OFF
Wire Wire Line
	4900 5900 4750 5900
Wire Wire Line
	4750 5550 4600 5550
Wire Wire Line
	4750 5550 5050 5550
Connection ~ 4750 5550
Wire Wire Line
	5200 5900 5350 5900
Wire Wire Line
	5350 5750 5350 5900
Wire Wire Line
	4750 5550 4750 5900
Connection ~ 5350 5900
Text Label 5350 6050 0    50   ~ 0
GND_R
Text Label 5500 5200 0    50   ~ 0
ON_OFF
Text Notes 4050 5300 0    50   ~ 0
WAKE Reference Circuit
$Comp
L Device:C C?
U 1 1 614D0FA0
P 5700 5550
AR Path="/614D0FA0" Ref="C?"  Part="1" 
AR Path="/60C56809/614D0FA0" Ref="C?"  Part="1" 
AR Path="/6114C59C/614D0FA0" Ref="C39"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614D0FA0" Ref="C?"  Part="1" 
F 0 "C?" H 5815 5596 50  0000 L CNN
F 1 "33pF" H 5815 5505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5738 5400 50  0001 C CNN
F 3 "~" H 5700 5550 50  0001 C CNN
	1    5700 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5200 5700 5200
Wire Wire Line
	5700 5200 5700 5400
Wire Wire Line
	5700 5700 5700 5900
Wire Wire Line
	5700 5900 5350 5900
$Comp
L Device:R R?
U 1 1 614D0FAA
P 5350 4900
AR Path="/614D0FAA" Ref="R?"  Part="1" 
AR Path="/60C56809/614D0FAA" Ref="R?"  Part="1" 
AR Path="/6114C59C/614D0FAA" Ref="R49"  Part="1" 
AR Path="/6083CF6C/6083E0B5/614D0FAA" Ref="R?"  Part="1" 
F 0 "R?" H 5280 4854 50  0000 R CNN
F 1 "4.7k" H 5280 4945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 4900 50  0001 C CNN
F 3 "~" H 5350 4900 50  0001 C CNN
	1    5350 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4750 5350 4600
Wire Wire Line
	5350 5050 5350 5200
Connection ~ 5350 5200
Text Label 5350 4600 0    50   ~ 0
VDD_EXT
Text Label 2750 1750 2    50   ~ 0
S_LED
Wire Wire Line
	2900 1750 2750 1750
NoConn ~ 4600 3400
NoConn ~ 4800 3400
NoConn ~ 4750 1900
NoConn ~ 4550 1900
NoConn ~ 4450 1900
NoConn ~ 4250 1900
NoConn ~ 6100 1750
NoConn ~ 6100 3150
NoConn ~ 6100 3250
NoConn ~ 6100 3350
NoConn ~ 6100 3550
NoConn ~ 5200 4300
NoConn ~ 4400 4300
NoConn ~ 4500 4300
NoConn ~ 4600 4300
NoConn ~ 4700 4300
NoConn ~ 4800 4300
NoConn ~ 4900 4300
Text Label 1200 2050 0    50   ~ 0
NB_WAKE
Wire Wire Line
	1050 2050 1200 2050
Text HLabel 1050 2050 0    50   Output ~ 0
WAKE
Text Label 1200 1900 0    50   ~ 0
BJT_CTRL
Wire Wire Line
	1050 1900 1200 1900
Text HLabel 1050 1900 0    50   Input ~ 0
BJT_CTRL_R
Text Label 2750 3450 2    50   ~ 0
VCC_R
Wire Wire Line
	2900 3450 2750 3450
Wire Wire Line
	3900 4450 3900 4300
Text Label 3900 4450 3    50   ~ 0
GND_R
Text Label 3800 4450 3    50   ~ 0
VCC_R
Wire Wire Line
	3800 4300 3800 4450
Connection ~ 8300 3750
Wire Wire Line
	8300 3750 8150 3750
Connection ~ 8700 3850
Connection ~ 9150 4150
Wire Wire Line
	9150 4150 8650 4150
Wire Wire Line
	9150 4150 9600 4150
Wire Wire Line
	8300 3750 9600 3750
Wire Wire Line
	8700 3850 9600 3850
Wire Wire Line
	9550 3650 9550 4500
Connection ~ 9550 3650
Wire Wire Line
	9550 3650 9600 3650
NoConn ~ 4000 4300
NoConn ~ 4100 4300
NoConn ~ 4200 4300
NoConn ~ 4300 4300
Wire Wire Line
	7500 950  7350 950 
Text Label 7350 950  2    50   ~ 0
ANT_IMN
Wire Wire Line
	4000 950  4000 800 
Text Label 4000 800  1    50   ~ 0
ANT_IMN
Wire Wire Line
	9100 3650 9450 3650
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 61891CB0
P 9450 3500
AR Path="/6114C59C/61891CB0" Ref="#FLG0103"  Part="1" 
AR Path="/6083CF6C/6083E0B5/61891CB0" Ref="#FLG?"  Part="1" 
F 0 "#FLG?" H 9450 3575 50  0001 C CNN
F 1 "PWR_FLAG" H 9450 3673 50  0000 C CNN
F 2 "" H 9450 3500 50  0001 C CNN
F 3 "~" H 9450 3500 50  0001 C CNN
	1    9450 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 3500 9450 3650
Connection ~ 9450 3650
Wire Wire Line
	9450 3650 9550 3650
Text Label 5000 4450 3    50   ~ 0
RXD
Text Label 5100 4450 3    50   ~ 0
TXD
Wire Wire Line
	9200 3950 9600 3950
Wire Wire Line
	1750 6450 1750 6300
Wire Wire Line
	1750 6900 1900 6900
Wire Wire Line
	1450 6900 1750 6900
Connection ~ 1750 6900
Wire Wire Line
	1750 6750 1750 6900
$Comp
L Device:R R?
U 1 1 60CFFAB5
P 1750 6600
AR Path="/60CFFAB5" Ref="R?"  Part="1" 
AR Path="/60C56809/60CFFAB5" Ref="R16"  Part="1" 
AR Path="/6114C59C/60CFFAB5" Ref="R33"  Part="1" 
AR Path="/6083CF6C/6083E0B5/60CFFAB5" Ref="R?"  Part="1" 
F 0 "R?" H 1680 6554 50  0000 R CNN
F 1 "4.7k" H 1680 6645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1680 6600 50  0001 C CNN
F 3 "~" H 1750 6600 50  0001 C CNN
	1    1750 6600
	-1   0    0    -1  
$EndComp
Text HLabel 1050 1300 0    50   Input ~ 0
VCC_BATT_R
Wire Wire Line
	1050 1300 1200 1300
Text Label 1200 1300 0    50   ~ 0
NB_VCC_BATT
Text Label 1750 6300 0    50   ~ 0
NB_VCC_BATT
Text Label 9200 3950 2    50   ~ 0
GND_R
$Comp
L Device:D_Zener D7
U 1 1 61AC73AF
P 10450 2350
AR Path="/6114C59C/61AC73AF" Ref="D7"  Part="1" 
AR Path="/6083CF6C/6083E0B5/61AC73AF" Ref="D?"  Part="1" 
F 0 "D?" V 10404 2429 50  0000 L CNN
F 1 "TVS" V 10495 2429 50  0000 L CNN
F 2 "" H 10450 2350 50  0001 C CNN
F 3 "~" H 10450 2350 50  0001 C CNN
	1    10450 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	10450 2500 10450 2700
$EndSCHEMATC
