EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "WISE FARM COLLAR"
Date "2021-08-23"
Rev "1"
Comp "WISE FARM"
Comment1 "Vitor Canosa"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6700 5400 6850 5400
Wire Wire Line
	6850 4200 6700 4200
Text Label 2100 3000 0    60   ~ 0
VCC_BATT
Text Label 2100 3750 0    60   ~ 0
GND_BATT
Text Notes 1700 3400 0    59   ~ 0
CR123
$Comp
L power:GND #PWR01
U 1 1 60A6BA0C
P 2100 3750
AR Path="/60A6BA0C" Ref="#PWR01"  Part="1" 
AR Path="/608DC206/60A6BA0C" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 2100 3500 50  0001 C CNN
F 1 "GND" H 2105 3577 50  0000 C CNN
F 2 "" H 2100 3750 50  0001 C CNN
F 3 "" H 2100 3750 50  0001 C CNN
	1    2100 3750
	1    0    0    -1  
$EndComp
$Sheet
S 6850 2500 800  1300
U 60886A89
F0 "LIS2DW12" 50
F1 "accelerometer/LIS2DW12.sch" 50
F2 "CS_ACC" I L 6850 3050 50 
F3 "SCL_ACC" I L 6850 3150 50 
F4 "SDO_ACC" O L 6850 2950 50 
F5 "SDI_ACC" I L 6850 2850 50 
F6 "INT1_ACC" O L 6850 3250 50 
F7 "INT2_ACC" O L 6850 3350 50 
F8 "VDD_ACC_IO" I L 6850 2700 50 
F9 "GND_ACC" I L 6850 3700 50 
F10 "VDD_ACC" I L 6850 2600 50 
$EndSheet
Wire Wire Line
	4950 2850 6850 2850
Wire Wire Line
	4950 2950 6850 2950
Wire Wire Line
	4950 3050 6850 3050
Wire Wire Line
	4950 3250 6850 3250
Wire Wire Line
	4950 3350 6850 3350
Wire Wire Line
	4950 3150 6850 3150
Wire Wire Line
	3700 1700 3550 1700
Wire Wire Line
	3700 4750 3550 4750
Text Label 3550 4750 2    60   ~ 0
GND_BATT
Wire Wire Line
	4950 5250 6850 5250
Wire Wire Line
	6850 5150 4950 5150
Wire Wire Line
	4950 4750 6850 4750
Wire Wire Line
	4950 4650 6850 4650
Wire Wire Line
	4950 4500 6850 4500
Wire Wire Line
	6850 1700 6700 1700
Text Label 6700 1700 2    60   ~ 0
VCC_BATT
Wire Wire Line
	6700 2100 6850 2100
$Sheet
S 6850 1600 1100 600 
U 608585CB
F0 "power_supply" 50
F1 "power_supply/power_supply.sch" 50
F2 "VIN" I L 6850 1700 50 
F3 "GND" I L 6850 2100 50 
F4 "EN_DC_DC_R" I L 6850 1850 50 
F5 "EN_LDO_R" I L 6850 1950 50 
F6 "VOUT_R" O R 7950 1850 50 
F7 "VOUT_A" O R 7950 1950 50 
$EndSheet
Text Label 6700 2100 2    60   ~ 0
GND_BATT
Wire Wire Line
	6850 2600 6700 2600
Wire Wire Line
	6850 2700 6700 2700
Text Label 6700 5400 2    60   ~ 0
GND_BATT
Wire Wire Line
	6700 3700 6850 3700
Text Label 6700 3700 2    60   ~ 0
GND_BATT
$Comp
L Device:Battery_Cell BT2
U 1 1 60A6BA13
P 2100 3500
AR Path="/60A6BA13" Ref="BT2"  Part="1" 
AR Path="/608DC206/60A6BA13" Ref="BT?"  Part="1" 
F 0 "BT2" H 2200 3550 50  0000 L CNN
F 1 "Battery" H 2150 3350 50  0000 L CNN
F 2 "custom:1051_CR123A_Key" V 2100 3540 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/mpd-memory-protection-devices/BK-5033/BK-5033-ND/2330512" H 2100 3540 50  0001 C CNN
F 4 "BK-5033-ND" H 2100 3500 60  0001 C CNN "digikey"
	1    2100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3000 2100 3300
Wire Wire Line
	2100 3750 2100 3600
$Sheet
S 3700 1600 1250 4250
U 608DC206
F0 "MCU" 50
F1 "mcu/STM32L412KBT6.sch" 50
F2 "VCC_M" I L 3700 1700 50 
F3 "GND_M" I L 3700 4750 50 
F4 "RESET_R" I R 4950 4500 50 
F5 "TXD_R" I R 4950 4650 50 
F6 "RXD_R" I R 4950 4750 50 
F7 "UCR_R" I R 4950 5250 50 
F8 "SDI_ACC_MCU" O R 4950 2850 50 
F9 "SDO_ACC_MCU" I R 4950 2950 50 
F10 "CS_ACC_MCU" O R 4950 3050 50 
F11 "SCL_ACC_MCU" O R 4950 3150 50 
F12 "INT1_ACC_MCU" I R 4950 3250 50 
F13 "INT2_ACC_MCU" I R 4950 3350 50 
F14 "EN_DC_DC_PS" O R 4950 1850 50 
F15 "EN_LDO_PS" O R 4950 1950 50 
F16 "PWRKEY_R" I R 4950 5150 50 
F17 "TXD_GPS" I R 4950 4900 50 
F18 "RXD_GPS" I R 4950 5000 50 
F19 "VCC_EMU" I L 3700 1800 50 
$EndSheet
$Comp
L Connector:TestPoint TP33
U 1 1 60C39940
P 9650 1750
AR Path="/60C39940" Ref="TP33"  Part="1" 
AR Path="/60C56809/60C39940" Ref="TP?"  Part="1" 
AR Path="/6114C59C/60C39940" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60C39940" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60C39940" Ref="TP?"  Part="1" 
AR Path="/608585CB/60C39940" Ref="TP?"  Part="1" 
F 0 "TP33" V 9650 1938 50  0000 L CNN
F 1 "TestPoint" H 9708 1777 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 1750 50  0001 C CNN
F 3 "~" H 9850 1750 50  0001 C CNN
	1    9650 1750
	0    1    -1   0   
$EndComp
Wire Wire Line
	9650 1750 9500 1750
Text Label 9500 1750 2    60   ~ 0
VCC_BATT
Wire Wire Line
	9650 1850 9500 1850
$Comp
L Connector:TestPoint TP34
U 1 1 60C3F13F
P 9650 1850
AR Path="/60C3F13F" Ref="TP34"  Part="1" 
AR Path="/60C56809/60C3F13F" Ref="TP?"  Part="1" 
AR Path="/6114C59C/60C3F13F" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60C3F13F" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60C3F13F" Ref="TP?"  Part="1" 
AR Path="/608585CB/60C3F13F" Ref="TP?"  Part="1" 
F 0 "TP34" V 9650 2038 50  0000 L CNN
F 1 "TestPoint" H 9708 1877 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 1850 50  0001 C CNN
F 3 "~" H 9850 1850 50  0001 C CNN
	1    9650 1850
	0    1    -1   0   
$EndComp
Text Label 9500 1850 2    60   ~ 0
VCC_BATT
Wire Wire Line
	7950 1850 8100 1850
Text Label 8100 1850 0    60   ~ 0
VCC_3v6
Text Label 6700 2600 2    60   ~ 0
VCC_1v8
$Sheet
S 6850 4100 800  1400
U 60BA0495
F0 "BG95_M3" 50
F1 "radio/BG95_M3.sch" 50
F2 "VCC_R" I L 6850 4200 50 
F3 "GND_R" I L 6850 5400 50 
F4 "RESET_R" I L 6850 4500 50 
F5 "TXD_R" O L 6850 4650 50 
F6 "RXD_R" I L 6850 4750 50 
F7 "UCR_R" O L 6850 5250 50 
F8 "PWRKEY_R" I L 6850 5150 50 
F9 "TXD_GPS" O L 6850 4900 50 
F10 "RXD_GPS" I L 6850 5000 50 
$EndSheet
Wire Wire Line
	7950 1950 8100 1950
Text Label 8100 1950 0    60   ~ 0
VCC_1v8
Text Label 6700 4200 2    60   ~ 0
VCC_3v6
Wire Wire Line
	4950 1950 6850 1950
Wire Wire Line
	4950 1850 6850 1850
Text Label 9500 2150 2    60   ~ 0
VCC_3v6
$Comp
L Connector:TestPoint TP36
U 1 1 60DE8B5B
P 9650 2150
AR Path="/60DE8B5B" Ref="TP36"  Part="1" 
AR Path="/60C56809/60DE8B5B" Ref="TP?"  Part="1" 
AR Path="/6114C59C/60DE8B5B" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/60DE8B5B" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/60DE8B5B" Ref="TP?"  Part="1" 
AR Path="/608585CB/60DE8B5B" Ref="TP?"  Part="1" 
F 0 "TP36" V 9650 2338 50  0000 L CNN
F 1 "TestPoint" H 9708 2177 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 2150 50  0001 C CNN
F 3 "~" H 9850 2150 50  0001 C CNN
	1    9650 2150
	0    1    -1   0   
$EndComp
Wire Wire Line
	9650 2150 9500 2150
Text Label 9500 2050 2    60   ~ 0
VCC_3v6
$Comp
L Connector:TestPoint TP28
U 1 1 61242ACD
P 9650 2050
AR Path="/61242ACD" Ref="TP28"  Part="1" 
AR Path="/60C56809/61242ACD" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61242ACD" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61242ACD" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61242ACD" Ref="TP?"  Part="1" 
AR Path="/608585CB/61242ACD" Ref="TP?"  Part="1" 
F 0 "TP28" V 9650 2238 50  0000 L CNN
F 1 "TestPoint" H 9708 2077 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 2050 50  0001 C CNN
F 3 "~" H 9850 2050 50  0001 C CNN
	1    9650 2050
	0    1    -1   0   
$EndComp
Wire Wire Line
	9650 2050 9500 2050
Wire Wire Line
	4950 5000 6850 5000
Wire Wire Line
	6850 4900 4950 4900
Text Label 6700 2700 2    60   ~ 0
VCC_1v8
Text Label 3550 1800 2    60   ~ 0
VCC_BATT
Text Label 3550 1700 2    60   ~ 0
VCC_1v8
Wire Wire Line
	3700 1800 3550 1800
Wire Wire Line
	9650 2350 9500 2350
$Comp
L Connector:TestPoint TP27
U 1 1 6184D463
P 9650 2350
AR Path="/6184D463" Ref="TP27"  Part="1" 
AR Path="/60C56809/6184D463" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6184D463" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6184D463" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6184D463" Ref="TP?"  Part="1" 
AR Path="/608585CB/6184D463" Ref="TP?"  Part="1" 
AR Path="/608DC206/6184D463" Ref="TP?"  Part="1" 
F 0 "TP27" V 9650 2538 50  0000 L CNN
F 1 "TestPoint" H 9708 2377 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 2350 50  0001 C CNN
F 3 "~" H 9850 2350 50  0001 C CNN
	1    9650 2350
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP29
U 1 1 6184D469
P 9650 2450
AR Path="/6184D469" Ref="TP29"  Part="1" 
AR Path="/60C56809/6184D469" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6184D469" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6184D469" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6184D469" Ref="TP?"  Part="1" 
AR Path="/608585CB/6184D469" Ref="TP?"  Part="1" 
AR Path="/608DC206/6184D469" Ref="TP?"  Part="1" 
F 0 "TP29" V 9650 2638 50  0000 L CNN
F 1 "TestPoint" H 9708 2477 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 9850 2450 50  0001 C CNN
F 3 "~" H 9850 2450 50  0001 C CNN
	1    9650 2450
	0    1    -1   0   
$EndComp
Wire Wire Line
	9650 2450 9500 2450
Text Label 9500 2350 2    60   ~ 0
VCC_1v8
Text Label 9500 2450 2    60   ~ 0
VCC_1v8
$EndSCHEMATC
