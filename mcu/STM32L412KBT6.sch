EESchema Schematic File Version 4
LIBS:wise_farm_nb_iot_2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "MCU"
Date "2021-08-23"
Rev "1"
Comp ""
Comment1 "Microcontroller"
Comment2 "Vitor Canosa"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D?
U 1 1 608E35E4
P 3450 5200
AR Path="/608E35E4" Ref="D?"  Part="1" 
AR Path="/608DC206/608E35E4" Ref="D2"  Part="1" 
F 0 "D2" V 3489 5083 50  0000 R CNN
F 1 "LED" V 3398 5083 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3450 5200 50  0001 C CNN
F 3 "~" H 3450 5200 50  0001 C CNN
F 4 "LTST-C190KRKT-TH" H 3450 5200 50  0001 C CNN "MPN"
	1    3450 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60A6BA08
P 3450 4700
AR Path="/60A6BA08" Ref="R?"  Part="1" 
AR Path="/608DC206/60A6BA08" Ref="R4"  Part="1" 
F 0 "R4" H 3520 4746 50  0000 L CNN
F 1 "4.7k" H 3520 4655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3380 4700 50  0001 C CNN
F 3 "~" H 3450 4700 50  0001 C CNN
	1    3450 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4850 3450 5050
Wire Wire Line
	3450 5350 3450 5500
Wire Wire Line
	3450 4550 3450 4400
Text Label 3450 4400 0    50   ~ 0
LED1
Text Label 4650 4250 2    50   ~ 0
SWDCLK
Wire Wire Line
	5950 5150 5950 5300
Wire Wire Line
	5850 2650 5850 2500
Text Label 4650 4150 2    50   ~ 0
SWDIO
Wire Wire Line
	4800 4150 4650 4150
Wire Wire Line
	4800 4250 4650 4250
Wire Wire Line
	7600 3950 7750 3950
Text HLabel 1850 600  0    50   Input ~ 0
VCC_M
Text HLabel 1850 700  0    50   Input ~ 0
GND_M
Text Label 2000 600  0    50   ~ 0
VCC_MCU
Text Label 2000 700  0    50   ~ 0
GND_MCU
Wire Wire Line
	1850 600  2000 600 
Wire Wire Line
	1850 700  2000 700 
Text Label 5850 2500 1    50   ~ 0
GND_MCU
Text Label 5100 1300 0    50   ~ 0
INT1_ACC_MCU
Text Label 5100 1100 0    50   ~ 0
CS_ACC_MCU
Text Label 5100 1000 0    50   ~ 0
SDO_ACC_MCU
Text Label 5100 900  0    50   ~ 0
SDI_ACC_MCU
Text Label 5100 1200 0    50   ~ 0
SCL_ACC_MCU
Wire Wire Line
	4950 1100 5100 1100
Wire Wire Line
	4950 1400 5100 1400
Wire Wire Line
	4950 1000 5100 1000
Wire Wire Line
	4950 900  5100 900 
Text Label 5100 1400 0    50   ~ 0
INT2_ACC_MCU
Wire Wire Line
	4950 1300 5100 1300
Wire Wire Line
	4950 1200 5100 1200
Text HLabel 4950 900  0    50   Output ~ 0
SDI_ACC_MCU
Text HLabel 4950 1000 0    50   Input ~ 0
SDO_ACC_MCU
Text HLabel 4950 1100 0    50   Output ~ 0
CS_ACC_MCU
Text HLabel 4950 1200 0    50   Output ~ 0
SCL_ACC_MCU
Text HLabel 4950 1300 0    50   Input ~ 0
INT1_ACC_MCU
Text HLabel 4950 1400 0    50   Input ~ 0
INT2_ACC_MCU
Text Label 3250 1000 0    50   ~ 0
RESET_RADIO
Text Label 3250 1100 0    50   ~ 0
TXD_RADIO
Text Label 3250 1200 0    50   ~ 0
RXD_RADIO
Text Label 3250 1400 0    50   ~ 0
UCR_RADIO
Text HLabel 3100 1000 0    50   Input ~ 0
RESET_R
Wire Wire Line
	3100 1000 3250 1000
Text HLabel 3100 1100 0    50   Input ~ 0
TXD_R
Wire Wire Line
	3100 1100 3250 1100
Text HLabel 3100 1200 0    50   Input ~ 0
RXD_R
Wire Wire Line
	3100 1200 3250 1200
Text HLabel 3100 1300 0    50   Input ~ 0
PWRKEY_R
Wire Wire Line
	3100 1300 3250 1300
Text HLabel 3100 1400 0    50   Input ~ 0
UCR_R
Wire Wire Line
	3100 1400 3250 1400
Text Label 2000 1050 0    50   ~ 0
EN_DC_DC
Text HLabel 1850 1050 0    50   Output ~ 0
EN_DC_DC_PS
Wire Wire Line
	1850 1050 2000 1050
Text Label 3450 5500 0    50   ~ 0
GND_MCU
$Comp
L custom:conn_2x5_1.27mm X?
U 1 1 60B28F54
P 8100 1150
AR Path="/60A9EC84/60B28F54" Ref="X?"  Part="1" 
AR Path="/608DC206/60B28F54" Ref="X1"  Part="1" 
F 0 "X1" H 8100 1597 60  0000 C CNN
F 1 "conn_2x5_1.27mm" H 8100 1491 60  0000 C CNN
F 2 "custom:CON1_27MMSMD10P" H 8100 1491 60  0001 C CNN
F 3 "" H 8150 1450 60  0000 C CNN
	1    8100 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 950  8550 950 
Wire Wire Line
	8400 1050 8550 1050
Wire Wire Line
	8400 1250 8550 1250
Wire Wire Line
	8400 1350 8550 1350
Wire Wire Line
	7800 950  7650 950 
Wire Wire Line
	7800 1050 7650 1050
Wire Wire Line
	7800 1150 7650 1150
Wire Wire Line
	7800 1250 7650 1250
Wire Wire Line
	7800 1350 7650 1350
Text Label 8550 950  0    50   ~ 0
GND
Text Label 7650 1250 2    50   ~ 0
SWDCLK
Text Label 7650 1350 2    50   ~ 0
SWDIO
NoConn ~ 7650 1050
NoConn ~ 8550 1050
NoConn ~ 8550 1250
Wire Wire Line
	8400 1150 9100 1150
Text Notes 8900 950  0    59   ~ 0
JLink-Lite programmer maintains RESET pin LOW\nSo this resistor must be unmounted if \nJLink-Lite programmer is used
$Comp
L custom:STM32L412KBT6 U5
U 1 1 608B9E86
P 7600 4650
F 0 "U5" H 9000 3900 60  0000 C CNN
F 1 "STM32L412KBT6" H 9050 3750 60  0000 C CNN
F 2 "custom:STM32L412KBT6" H 9000 4890 60  0001 C CNN
F 3 "" H 7600 4650 60  0000 C CNN
	1    7600 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6550 5150 6550 5300
Text Label 6550 5300 3    50   ~ 0
GND_MCU
Text Label 8650 1150 0    50   ~ 0
HD_RESETn
Wire Wire Line
	6350 5150 6350 5300
Wire Wire Line
	6250 5150 6250 5300
Wire Wire Line
	6150 5150 6150 5300
Text Label 4650 4050 2    50   ~ 0
SDI_ACC_MCU
Text Label 4650 3950 2    50   ~ 0
SDO_ACC_MCU
Text Label 5850 5300 3    50   ~ 0
CS_ACC_MCU
Wire Wire Line
	7600 3750 7750 3750
Wire Wire Line
	7600 3650 7750 3650
Wire Wire Line
	7600 3550 7750 3550
Text Label 7750 3750 0    50   ~ 0
INT1_ACC_MCU
Text Label 5950 5300 3    50   ~ 0
SCL_ACC_MCU
Wire Wire Line
	4650 4050 4800 4050
Wire Wire Line
	4800 3950 4650 3950
Text Label 6050 2500 1    50   ~ 0
PWRKEY_RADIO
Wire Wire Line
	4800 3850 4650 3850
Text Label 6550 2500 1    50   ~ 0
RXD_RADIO
Wire Wire Line
	4800 3750 4650 3750
Wire Wire Line
	4800 3650 4650 3650
Text Label 7750 3550 0    50   ~ 0
TXD_RADIO
Wire Wire Line
	4800 3550 4650 3550
Text Label 4650 3550 2    50   ~ 0
VCC_MCU
Wire Wire Line
	7600 4250 7750 4250
Text Label 7750 4250 0    50   ~ 0
VCC_MCU
Wire Wire Line
	6150 2650 6150 2500
Wire Wire Line
	6550 2650 6550 2500
Wire Wire Line
	6250 2650 6250 2500
Wire Wire Line
	6350 2500 6350 2650
Wire Wire Line
	6450 2650 6450 2500
NoConn ~ 7600 4050
NoConn ~ 7600 4150
NoConn ~ 6450 5150
Wire Wire Line
	5850 5150 5850 5300
Wire Wire Line
	6050 5150 6050 5300
Text HLabel 1850 1150 0    50   Output ~ 0
EN_LDO_PS
Wire Wire Line
	1850 1150 2000 1150
Text Label 2000 1150 0    50   ~ 0
EN_LDO
NoConn ~ 7650 1150
Text Notes 2500 750  0    59   ~ 0
When emulator is connected it supplies 3v3 for programming\nBattery must not be connected !!!
$Comp
L Device:R R?
U 1 1 60D018BA
P 4050 6700
AR Path="/60D018BA" Ref="R?"  Part="1" 
AR Path="/608DC206/60D018BA" Ref="R10"  Part="1" 
F 0 "R10" H 4120 6746 50  0000 L CNN
F 1 "4.7k" H 4120 6655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3980 6700 50  0001 C CNN
F 3 "~" H 4050 6700 50  0001 C CNN
	1    4050 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 6850 4050 7050
Wire Wire Line
	4050 6550 4050 6400
Text Label 4050 7050 0    50   ~ 0
GND_MCU
Text Label 4050 6400 0    50   ~ 0
INT1_ACC_MCU
Text Notes 4400 6800 0    50   ~ 0
INT1 Pull down\nMount or Not mount\nNeed testing
Wire Wire Line
	7600 3850 7750 3850
Text Label 7750 3850 0    50   ~ 0
VCC_MCU
Text Label 7650 950  2    50   ~ 0
GND
Wire Wire Line
	1850 7550 1700 7550
$Comp
L Connector:TestPoint TP?
U 1 1 611FD503
P 1850 7550
AR Path="/611FD503" Ref="TP?"  Part="1" 
AR Path="/60C56809/611FD503" Ref="TP?"  Part="1" 
AR Path="/6114C59C/611FD503" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/611FD503" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/611FD503" Ref="TP?"  Part="1" 
AR Path="/608585CB/611FD503" Ref="TP?"  Part="1" 
AR Path="/608DC206/611FD503" Ref="TP10"  Part="1" 
F 0 "TP10" V 1850 7738 50  0000 L CNN
F 1 "TestPoint" H 1908 7577 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2050 7550 50  0001 C CNN
F 3 "~" H 2050 7550 50  0001 C CNN
	1    1850 7550
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 611FD509
P 1850 7450
AR Path="/611FD509" Ref="TP?"  Part="1" 
AR Path="/60C56809/611FD509" Ref="TP?"  Part="1" 
AR Path="/6114C59C/611FD509" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/611FD509" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/611FD509" Ref="TP?"  Part="1" 
AR Path="/608585CB/611FD509" Ref="TP?"  Part="1" 
AR Path="/608DC206/611FD509" Ref="TP9"  Part="1" 
F 0 "TP9" V 1850 7638 50  0000 L CNN
F 1 "TestPoint" H 1908 7477 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2050 7450 50  0001 C CNN
F 3 "~" H 2050 7450 50  0001 C CNN
	1    1850 7450
	0    1    -1   0   
$EndComp
Wire Wire Line
	1850 7450 1700 7450
Wire Wire Line
	1850 7250 1700 7250
$Comp
L Connector:TestPoint TP?
U 1 1 611FD513
P 1850 7250
AR Path="/611FD513" Ref="TP?"  Part="1" 
AR Path="/60C56809/611FD513" Ref="TP?"  Part="1" 
AR Path="/6114C59C/611FD513" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/611FD513" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/611FD513" Ref="TP?"  Part="1" 
AR Path="/608585CB/611FD513" Ref="TP?"  Part="1" 
AR Path="/608DC206/611FD513" Ref="TP2"  Part="1" 
F 0 "TP2" V 1850 7438 50  0000 L CNN
F 1 "TestPoint" H 1908 7277 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2050 7250 50  0001 C CNN
F 3 "~" H 2050 7250 50  0001 C CNN
	1    1850 7250
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 611FD519
P 1850 7150
AR Path="/611FD519" Ref="TP?"  Part="1" 
AR Path="/60C56809/611FD519" Ref="TP?"  Part="1" 
AR Path="/6114C59C/611FD519" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/611FD519" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/611FD519" Ref="TP?"  Part="1" 
AR Path="/608585CB/611FD519" Ref="TP?"  Part="1" 
AR Path="/608DC206/611FD519" Ref="TP1"  Part="1" 
F 0 "TP1" V 1850 7338 50  0000 L CNN
F 1 "TestPoint" H 1908 7177 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2050 7150 50  0001 C CNN
F 3 "~" H 2050 7150 50  0001 C CNN
	1    1850 7150
	0    1    -1   0   
$EndComp
Wire Wire Line
	1850 7150 1700 7150
Text Label 1700 7150 2    50   ~ 0
SWDIO
Text Label 1700 7450 2    50   ~ 0
SWDCLK
Text Label 1700 7250 2    50   ~ 0
SWDIO
Text Label 1700 7550 2    50   ~ 0
SWDCLK
Wire Wire Line
	6050 2650 6050 2500
Wire Wire Line
	5950 2650 5950 2500
Wire Wire Line
	2900 7450 2750 7450
$Comp
L Connector:TestPoint TP?
U 1 1 6123CF3B
P 2900 7450
AR Path="/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/60C56809/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/608585CB/6123CF3B" Ref="TP?"  Part="1" 
AR Path="/608DC206/6123CF3B" Ref="TP20"  Part="1" 
F 0 "TP20" V 2900 7638 50  0000 L CNN
F 1 "TestPoint" H 2958 7477 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 3100 7450 50  0001 C CNN
F 3 "~" H 3100 7450 50  0001 C CNN
	1    2900 7450
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6123CF41
P 2900 7550
AR Path="/6123CF41" Ref="TP?"  Part="1" 
AR Path="/60C56809/6123CF41" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6123CF41" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6123CF41" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6123CF41" Ref="TP?"  Part="1" 
AR Path="/608585CB/6123CF41" Ref="TP?"  Part="1" 
AR Path="/608DC206/6123CF41" Ref="TP22"  Part="1" 
F 0 "TP22" V 2900 7738 50  0000 L CNN
F 1 "TestPoint" H 2958 7577 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 3100 7550 50  0001 C CNN
F 3 "~" H 3100 7550 50  0001 C CNN
	1    2900 7550
	0    1    -1   0   
$EndComp
Wire Wire Line
	2900 7550 2750 7550
Text Label 2750 7450 2    50   ~ 0
EN_DC_DC
Text Label 2750 7550 2    50   ~ 0
EN_DC_DC
Text Label 6450 2500 1    50   ~ 0
EN_LDO
Wire Wire Line
	2900 7150 2750 7150
$Comp
L Connector:TestPoint TP?
U 1 1 6125E266
P 2900 7150
AR Path="/6125E266" Ref="TP?"  Part="1" 
AR Path="/60C56809/6125E266" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6125E266" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6125E266" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6125E266" Ref="TP?"  Part="1" 
AR Path="/608585CB/6125E266" Ref="TP?"  Part="1" 
AR Path="/608DC206/6125E266" Ref="TP12"  Part="1" 
F 0 "TP12" V 2900 7338 50  0000 L CNN
F 1 "TestPoint" H 2958 7177 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 3100 7150 50  0001 C CNN
F 3 "~" H 3100 7150 50  0001 C CNN
	1    2900 7150
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6125E26C
P 2900 7250
AR Path="/6125E26C" Ref="TP?"  Part="1" 
AR Path="/60C56809/6125E26C" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6125E26C" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6125E26C" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6125E26C" Ref="TP?"  Part="1" 
AR Path="/608585CB/6125E26C" Ref="TP?"  Part="1" 
AR Path="/608DC206/6125E26C" Ref="TP16"  Part="1" 
F 0 "TP16" V 2900 7438 50  0000 L CNN
F 1 "TestPoint" H 2958 7277 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 3100 7250 50  0001 C CNN
F 3 "~" H 3100 7250 50  0001 C CNN
	1    2900 7250
	0    1    -1   0   
$EndComp
Wire Wire Line
	2900 7250 2750 7250
Text Label 2750 7150 2    50   ~ 0
EN_LDO
Text Label 2750 7250 2    50   ~ 0
EN_LDO
Text Label 7750 3650 0    50   ~ 0
UCR_RADIO
Wire Wire Line
	2900 7650 2750 7650
$Comp
L Connector:TestPoint TP?
U 1 1 61274221
P 2900 7650
AR Path="/61274221" Ref="TP?"  Part="1" 
AR Path="/60C56809/61274221" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61274221" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61274221" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61274221" Ref="TP?"  Part="1" 
AR Path="/608585CB/61274221" Ref="TP?"  Part="1" 
AR Path="/608DC206/61274221" Ref="TP17"  Part="1" 
F 0 "TP17" V 2900 7838 50  0000 L CNN
F 1 "TestPoint" H 2958 7677 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 3100 7650 50  0001 C CNN
F 3 "~" H 3100 7650 50  0001 C CNN
	1    2900 7650
	0    1    -1   0   
$EndComp
NoConn ~ 6050 5300
NoConn ~ 6150 5300
NoConn ~ 6250 5300
NoConn ~ 6350 5300
NoConn ~ 9100 1150
Text Label 3250 1300 0    50   ~ 0
PWRKEY_RADIO
Wire Wire Line
	3100 1650 3250 1650
Wire Wire Line
	3100 1550 3250 1550
Text HLabel 3100 1650 0    50   Input ~ 0
RXD_GPS
Text Label 6150 2500 1    50   ~ 0
RESET_RADIO
Text Label 6250 2500 1    50   ~ 0
LED1
Text Label 3250 1650 0    50   ~ 0
RXD_GPS
Text Label 3250 1550 0    50   ~ 0
TXD_GPS
Text Notes 8200 1750 0    59   ~ 0
PS: PA6 - PIN12 is used to measure battery voltage
NoConn ~ 7750 3950
Text HLabel 3100 1550 0    50   Input ~ 0
TXD_GPS
Text Label 4650 3850 2    50   ~ 0
TXD_GPS
Text Label 4650 3750 2    50   ~ 0
RXD_GPS
Text Label 2750 7650 2    50   ~ 0
EN_DC_DC
Text HLabel 1850 800  0    50   Input ~ 0
VCC_EMU
Text Label 2000 800  0    50   ~ 0
VCC_EMU
Wire Wire Line
	1850 800  2000 800 
Text Label 8550 1350 0    50   ~ 0
VCC_EMU
Text Notes 850  6000 0    50   ~ 0
Level shifter is needed here because MCU is working @ 1.8V\nbut to enable DC/DC converter we at least 0.8 VBATT to turn on\nDC/DC boost converter
Text Label 6350 2500 1    50   ~ 0
EN_DC_DC_LS
$Comp
L Device:R R?
U 1 1 619EAC4A
P 3100 2700
AR Path="/619EAC4A" Ref="R?"  Part="1" 
AR Path="/608DC206/619EAC4A" Ref="R17"  Part="1" 
F 0 "R17" H 3170 2746 50  0000 L CNN
F 1 "10k" H 3170 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3030 2700 50  0001 C CNN
F 3 "~" H 3100 2700 50  0001 C CNN
	1    3100 2700
	1    0    0    -1  
$EndComp
Text Label 2550 3600 0    50   ~ 0
GND_MCU
$Comp
L Device:R R?
U 1 1 61A02C43
P 3100 3300
AR Path="/61A02C43" Ref="R?"  Part="1" 
AR Path="/608DC206/61A02C43" Ref="R18"  Part="1" 
F 0 "R18" H 3170 3346 50  0000 L CNN
F 1 "10k" H 3170 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3030 3300 50  0001 C CNN
F 3 "~" H 3100 3300 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3450 3100 3600
Wire Wire Line
	3100 3150 3100 3000
Wire Wire Line
	3100 3000 3350 3000
Connection ~ 3100 3000
Wire Wire Line
	3100 3000 3100 2850
Text Label 4650 3650 2    50   ~ 0
EN_BATT_MEAS
$Comp
L Device:R R15
U 1 1 5EB43960
P 1950 3300
F 0 "R15" H 1880 3254 50  0000 R CNN
F 1 "10k" H 1880 3345 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1880 3300 50  0001 C CNN
F 3 "~" H 1950 3300 50  0001 C CNN
F 4 "RS-03K1001FT" H 1950 3300 50  0001 C CNN "MPN"
F 5 "ST" H 1950 3300 50  0001 C CNN "FUNCTION"
	1    1950 3300
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q3
U 1 1 5F946D91
P 2300 2950
F 0 "Q3" H 2491 2996 50  0000 L CNN
F 1 "Q_NPN_BEC" H 2491 2905 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2500 3050 50  0001 C CNN
F 3 "~" H 2300 2950 50  0001 C CNN
F 4 "PMBT3904,215" H 2300 2950 50  0001 C CNN "MPN"
	1    2300 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5F94B08F
P 2400 2400
F 0 "R16" H 2330 2354 50  0000 R CNN
F 1 "30k" H 2330 2445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2330 2400 50  0001 C CNN
F 3 "~" H 2400 2400 50  0001 C CNN
	1    2400 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2950 1950 3150
$Comp
L Device:R R14
U 1 1 5F94794D
P 1650 2950
F 0 "R14" H 1580 2904 50  0000 R CNN
F 1 "10k" H 1580 2995 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1580 2950 50  0001 C CNN
F 3 "~" H 1650 2950 50  0001 C CNN
	1    1650 2950
	0    -1   -1   0   
$EndComp
Text Notes 8050 6950 0    118  ~ 24
W4COM CONFIDENTIAL
Text Notes 8050 6950 0    118  ~ 24
W4COM CONFIDENTIAL
Text Notes 8050 6950 0    118  ~ 24
W4COM CONFIDENTIAL
Wire Wire Line
	1950 3450 1950 3600
Wire Wire Line
	2400 3150 2400 3600
Wire Wire Line
	1950 2950 2100 2950
Wire Wire Line
	1800 2950 1950 2950
Connection ~ 1950 2950
Wire Wire Line
	1500 2950 1350 2950
Wire Wire Line
	2400 2750 2400 2650
Connection ~ 2400 3600
Wire Wire Line
	1950 3600 2400 3600
$Comp
L Device:Q_PMOS_GSD Q4
U 1 1 61AAB6D4
P 2750 2200
F 0 "Q4" V 3093 2200 50  0000 C CNN
F 1 "Q_PMOS_GSD" V 3002 2200 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-883" H 2950 2300 50  0001 C CNN
F 3 "~" H 2750 2200 50  0001 C CNN
	1    2750 2200
	0    1    -1   0   
$EndComp
Wire Wire Line
	3100 3600 2400 3600
Wire Wire Line
	2950 2100 3100 2100
Wire Wire Line
	2550 2100 2400 2100
Wire Wire Line
	2750 2650 2400 2650
Connection ~ 2400 2650
Wire Wire Line
	2400 2550 2400 2650
Wire Wire Line
	2400 2100 2400 2250
Wire Wire Line
	3100 2100 3100 2550
Wire Wire Line
	2750 2400 2750 2650
Text Label 2250 2100 2    50   ~ 0
VCC_EMU
Wire Wire Line
	2400 2100 2250 2100
Connection ~ 2400 2100
Text Label 3350 3000 0    50   ~ 0
ADC_BATT_MEAS
Text Label 5950 2500 1    50   ~ 0
ADC_BATT_MEAS
Text Label 1350 2950 2    50   ~ 0
EN_BATT_MEAS
Wire Wire Line
	1900 6700 1750 6700
$Comp
L Connector:TestPoint TP?
U 1 1 61B15143
P 1900 6700
AR Path="/61B15143" Ref="TP?"  Part="1" 
AR Path="/60C56809/61B15143" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61B15143" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61B15143" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61B15143" Ref="TP?"  Part="1" 
AR Path="/608585CB/61B15143" Ref="TP?"  Part="1" 
AR Path="/608DC206/61B15143" Ref="TP13"  Part="1" 
F 0 "TP13" V 1900 6888 50  0000 L CNN
F 1 "TestPoint" H 1958 6727 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 2100 6700 50  0001 C CNN
F 3 "~" H 2100 6700 50  0001 C CNN
	1    1900 6700
	0    1    -1   0   
$EndComp
Text Label 1750 6700 2    50   ~ 0
VCC_EMU
Wire Wire Line
	6250 6950 6100 6950
$Comp
L Connector:TestPoint TP?
U 1 1 6181EAD0
P 6250 6950
AR Path="/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/60C56809/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/608585CB/6181EAD0" Ref="TP?"  Part="1" 
AR Path="/608DC206/6181EAD0" Ref="TP14"  Part="1" 
F 0 "TP14" V 6250 7138 50  0000 L CNN
F 1 "TestPoint" H 6308 6977 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 6950 50  0001 C CNN
F 3 "~" H 6450 6950 50  0001 C CNN
	1    6250 6950
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6181EAD6
P 6250 7050
AR Path="/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/60C56809/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/608585CB/6181EAD6" Ref="TP?"  Part="1" 
AR Path="/608DC206/6181EAD6" Ref="TP15"  Part="1" 
F 0 "TP15" V 6250 7238 50  0000 L CNN
F 1 "TestPoint" H 6308 7077 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 7050 50  0001 C CNN
F 3 "~" H 6450 7050 50  0001 C CNN
	1    6250 7050
	0    1    -1   0   
$EndComp
Wire Wire Line
	6250 7050 6100 7050
Wire Wire Line
	6250 7200 6100 7200
$Comp
L Connector:TestPoint TP?
U 1 1 6182278B
P 6250 7200
AR Path="/6182278B" Ref="TP?"  Part="1" 
AR Path="/60C56809/6182278B" Ref="TP?"  Part="1" 
AR Path="/6114C59C/6182278B" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/6182278B" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/6182278B" Ref="TP?"  Part="1" 
AR Path="/608585CB/6182278B" Ref="TP?"  Part="1" 
AR Path="/608DC206/6182278B" Ref="TP18"  Part="1" 
F 0 "TP18" V 6250 7388 50  0000 L CNN
F 1 "TestPoint" H 6308 7227 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 7200 50  0001 C CNN
F 3 "~" H 6450 7200 50  0001 C CNN
	1    6250 7200
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 61822791
P 6250 7300
AR Path="/61822791" Ref="TP?"  Part="1" 
AR Path="/60C56809/61822791" Ref="TP?"  Part="1" 
AR Path="/6114C59C/61822791" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/61822791" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/61822791" Ref="TP?"  Part="1" 
AR Path="/608585CB/61822791" Ref="TP?"  Part="1" 
AR Path="/608DC206/61822791" Ref="TP19"  Part="1" 
F 0 "TP19" V 6250 7488 50  0000 L CNN
F 1 "TestPoint" H 6308 7327 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 7300 50  0001 C CNN
F 3 "~" H 6450 7300 50  0001 C CNN
	1    6250 7300
	0    1    -1   0   
$EndComp
Wire Wire Line
	6250 7300 6100 7300
Text Label 6100 6950 2    50   ~ 0
ADC_BATT_MEAS
Text Label 6100 7050 2    50   ~ 0
ADC_BATT_MEAS
Text Label 6100 7200 2    50   ~ 0
PWRKEY_RADIO
Text Label 6100 7300 2    50   ~ 0
PWRKEY_RADIO
Wire Wire Line
	6250 7450 6100 7450
$Comp
L Connector:TestPoint TP?
U 1 1 618551E1
P 6250 7450
AR Path="/618551E1" Ref="TP?"  Part="1" 
AR Path="/60C56809/618551E1" Ref="TP?"  Part="1" 
AR Path="/6114C59C/618551E1" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/618551E1" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/618551E1" Ref="TP?"  Part="1" 
AR Path="/608585CB/618551E1" Ref="TP?"  Part="1" 
AR Path="/608DC206/618551E1" Ref="TP30"  Part="1" 
F 0 "TP30" V 6250 7638 50  0000 L CNN
F 1 "TestPoint" H 6308 7477 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 7450 50  0001 C CNN
F 3 "~" H 6450 7450 50  0001 C CNN
	1    6250 7450
	0    1    -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 618551E7
P 6250 7550
AR Path="/618551E7" Ref="TP?"  Part="1" 
AR Path="/60C56809/618551E7" Ref="TP?"  Part="1" 
AR Path="/6114C59C/618551E7" Ref="TP?"  Part="1" 
AR Path="/61CCC1A5/618551E7" Ref="TP?"  Part="1" 
AR Path="/6083CF6C/6083E0AA/618551E7" Ref="TP?"  Part="1" 
AR Path="/608585CB/618551E7" Ref="TP?"  Part="1" 
AR Path="/608DC206/618551E7" Ref="TP31"  Part="1" 
F 0 "TP31" V 6250 7738 50  0000 L CNN
F 1 "TestPoint" H 6308 7577 50  0001 L CNN
F 2 "custom:pad_hole_duallayer_board_D1.5mm" H 6450 7550 50  0001 C CNN
F 3 "~" H 6450 7550 50  0001 C CNN
	1    6250 7550
	0    1    -1   0   
$EndComp
Wire Wire Line
	6250 7550 6100 7550
Text Label 6100 7450 2    50   ~ 0
EN_BATT_MEAS
Text Label 6100 7550 2    50   ~ 0
EN_BATT_MEAS
$Comp
L Device:R R?
U 1 1 61B18221
P 2800 5250
AR Path="/61B18221" Ref="R?"  Part="1" 
AR Path="/608DC206/61B18221" Ref="R?"  Part="1" 
F 0 "R?" H 2870 5296 50  0000 L CNN
F 1 "10k" H 2870 5205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2730 5250 50  0001 C CNN
F 3 "~" H 2800 5250 50  0001 C CNN
	1    2800 5250
	1    0    0    -1  
$EndComp
Text Label 2250 5550 0    50   ~ 0
GND_MCU
Wire Wire Line
	2800 5400 2800 5550
$Comp
L Device:R R?
U 1 1 61B18235
P 1650 5250
F 0 "R?" H 1580 5204 50  0000 R CNN
F 1 "10k" H 1580 5295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1580 5250 50  0001 C CNN
F 3 "~" H 1650 5250 50  0001 C CNN
F 4 "RS-03K1001FT" H 1650 5250 50  0001 C CNN "MPN"
F 5 "ST" H 1650 5250 50  0001 C CNN "FUNCTION"
	1    1650 5250
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 61B1823C
P 2000 4900
F 0 "Q?" H 2191 4946 50  0000 L CNN
F 1 "Q_NPN_BEC" H 2191 4855 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2200 5000 50  0001 C CNN
F 3 "~" H 2000 4900 50  0001 C CNN
F 4 "PMBT3904,215" H 2000 4900 50  0001 C CNN "MPN"
	1    2000 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61B18242
P 2100 4350
F 0 "R?" H 2030 4304 50  0000 R CNN
F 1 "30k" H 2030 4395 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2030 4350 50  0001 C CNN
F 3 "~" H 2100 4350 50  0001 C CNN
	1    2100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4900 1650 5100
$Comp
L Device:R R?
U 1 1 61B18249
P 1350 4900
F 0 "R?" H 1280 4854 50  0000 R CNN
F 1 "10k" H 1280 4945 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1280 4900 50  0001 C CNN
F 3 "~" H 1350 4900 50  0001 C CNN
	1    1350 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 5400 1650 5550
Wire Wire Line
	2100 5100 2100 5550
Wire Wire Line
	1650 4900 1800 4900
Wire Wire Line
	1500 4900 1650 4900
Connection ~ 1650 4900
Wire Wire Line
	1200 4900 1050 4900
Wire Wire Line
	2100 4700 2100 4600
Connection ~ 2100 5550
Wire Wire Line
	1650 5550 2100 5550
$Comp
L Device:Q_PMOS_GSD Q?
U 1 1 61B18258
P 2700 4600
F 0 "Q?" V 3043 4600 50  0000 C CNN
F 1 "Q_PMOS_GSD" V 2952 4600 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-883" H 2900 4700 50  0001 C CNN
F 3 "~" H 2700 4600 50  0001 C CNN
	1    2700 4600
	1    0    0    1   
$EndComp
Wire Wire Line
	2800 5550 2100 5550
Connection ~ 2100 4600
Wire Wire Line
	2100 4500 2100 4600
Wire Wire Line
	2100 4050 2100 4200
Text Label 1950 4050 2    50   ~ 0
VCC_EMU
Wire Wire Line
	2100 4050 1950 4050
Connection ~ 2100 4050
Wire Wire Line
	2100 4600 2500 4600
Wire Wire Line
	2800 4400 2800 4050
Wire Wire Line
	2100 4050 2800 4050
Wire Wire Line
	2800 4800 2800 5100
Text Label 1050 4900 2    50   ~ 0
EN_DC_DC_LS
Text Label 2800 4950 0    50   ~ 0
EN_DC_DC
$EndSCHEMATC
